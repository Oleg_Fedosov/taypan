<?php
    namespace Modules\About\Controllers;

    use Core\Route;
    use Core\Config;
    use Core\View;

    use Modules\Content\Models\Control;
    
    class About extends \Modules\Base {

        public $current;

        public function before() {
            parent::before();
            $this->current = Control::getRow(Route::controller(), 'alias', 1);
            if( !$this->current ) {
                return Config::error();
            }
        }

        public function indexAction() {
            $this->_template = 'Template';
            // Seo
            $this->_seo['h1'] = $this->current->h1;
            $this->_seo['title'] = $this->current->title;
            $this->_seo['keywords'] = $this->current->keywords;
            $this->_seo['description'] = $this->current->description;
            $this->_seo['name'] = $this->current->name;

            $other = json_decode($this->current->other, true);

            // Render template
            $this->_content = View::tpl(array('text' => $this->current->text, 'image' => $this->current->image, 'other' => $other), 'About/Index');
        }
    }
    