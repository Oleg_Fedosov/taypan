<?php
    namespace Modules\Services\Controllers;

    use Core\Common;
    use Core\HTML;
    use Core\Route;
    use Core\View;
    use Core\Config;

    use Modules\Content\Models\Control;

    class Services extends \Modules\Base {

        public $current;
        public $model;

        public function before() {
            parent::before();
            $this->current = Control::getRow(Route::controller(), 'alias', 1);
            if( !$this->current ) {
                return Config::error();
            }
            $this->setBreadcrumbs($this->current->name, $this->current->alias);
            $this->model = Common::factory('services');
        }

        public function indexAction() {
            if( Config::get('error') ) {
                return false;
            }
            // Seo
            $this->_seo['h1'] = $this->current->h1;
            $this->_seo['title'] = $this->current->title;
            $this->_seo['keywords'] = $this->current->keywords;
            $this->_seo['description'] = $this->current->description;
            $this->_seo['name'] = $this->current->name;
            // Get Rows
            $result = $this->model->getRows(1, 'sort');
            // Render template
            $this->_content = View::tpl(array('result' => $result), 'Services/List');
        }

        public function innerAction() {
            if( Config::get('error') ) {
                return false;
            }
            $this->_template = 'Service';
            // Check for existance
            $obj = $this->model->getRow(Route::param('alias'), 'alias', 1);
            if( !$obj ) { return Config::error(); }
            // Seo
            $this->_seo['h1'] = $obj->h1;
            $this->_seo['title'] = $obj->title;
            $this->_seo['keywords'] = $obj->keywords;
            $this->_seo['description'] = $obj->description;
            $this->_seo['name'] = $obj->name;
            $this->setBreadcrumbs( $obj->name );
            // Add plus one to views
            $obj = $this->model->addView($obj);
            $this->seo();
            // Render template
            $this->_content = View::tpl(array('obj' => $obj, '_seo' => $this->_seo, 'breadcrumbs' => HTML::breadcrumbs($this->_breadcrumbs) ), 'Services/' . $obj->tpl);
        }
        
    }