<?php
    // Settings of images on the site
    return array(
        // Watermark path
        'watermark' => 'pic/logo.png',
        // Image types
        'types' => array(
            'jpg', 'jpeg', 'png', 'gif',
        ),
        // Banners images
        'banners' => array(
            array(
                'path' => '',
                'width' => 483,
                'height' => 160,
                'resize' => 1,
                'crop' => 1,
            ),
        ),
        // Stock images
        'stock' => array(
            array(
                'path' => 'small',
                'width' => 200,
                'height' => 70,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 1900,
                'height' => 600,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        // Slider images
        'slider' => array(
            array(
                'path' => 'small',
                'width' => 200,
                'height' => 70,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 1900,
                'height' => 670,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        // License images
        'license' => array(
            array(
                'path' => 'small',
                'width' => 100,
                'height' => 140,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'medium',
                'width' => 128,
                'height' => 184,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 1275,
                'height' => 1750,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        // Blog images
        'blog' => array(
            array(
                'path' => 'small',
                'width' => 200,
                'height' => 160,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 600,
                'height' => 400,
                'resize' => 1,
                'crop' => 0,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        // News images
        'news' => array(
            array(
                'path' => 'small',
                'width' => 378,
                'height' => 293,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 627,
                'height' => NULL,
                'resize' => 1,
                'crop' => 0,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        // Control images
        'control' => array(
            array(
                'path' => 'small',
                'width' => 378,
                'height' => 293,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 949,
                'height' => 723,
                'resize' => 1,
                'crop' => 0,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        // Services images
        'services' => array(
            array(
                'path' => 'small',
                'width' => 200,
                'height' => 160,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 959,
                'height' => 670,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        // Catalog groups images
        'catalog_tree' => array(
            array(
                'path' => '',
                'width' => 240,
                'height' => 240,
                'resize' => 1,
                'crop' => 1,
            ),
        ),
        // Products images
        'catalog' => array(
            array(
                'path' => 'small',
                'width' => 60,
                'height' => 60,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'medium',
                'width' => 232,
                'height' => 195,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 678,
                'height' => 520,
                'resize' => 1,
                'crop' => 0,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
        'gallery' => array(
            array(
                'path' => '',
                'width' => 200,
                'height' => 200,
                'resize' => 1,
                'crop' => 1,
            ),
        ),
        'gallery_images' => array(
            array(
                'path' => 'small',
                'width' => 200,
                'height' => 200,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'medium',
                'width' => 350,
                'height' => 350,
                'resize' => 1,
                'crop' => 1,
            ),
            array(
                'path' => 'big',
                'width' => 1280,
                'height' => 1024,
                'resize' => 1,
                'crop' => 0,
            ),
            array(
                'path' => 'original',
                'resize' => 0,
                'crop' => 0,
            ),
        ),
    );