/*init.js*/
/*
    init.js v2.0
    Wezom wTPL v4.0.0
*/
window.wHTML = (function($){

    /* Приватные переменные */

        var varSeoIframe = 'seoIframe',
            varSeoTxt = 'seoTxt',
            varSeoClone = 'cloneSeo',
            varSeoDelay = 200;

    /* Приватные функции */

        /* проверка типа данных на объект */
        var _isObject = function(data) {
            var flag = (typeof data == 'object') && (data+'' != 'null');
            return flag;
        },

        /* создание нового элемента элемента */
        _crtEl = function(tag, classes, attrs, jq) {
            var tagName = tag || 'div';
            var element = document.createElement(tagName);
            var jQueryElement = jq || true;
            // если классы объявлены - добавляем
            if (classes) {
                var tagClasses = classes.split(' ');
                for (var i = 0; i < tagClasses.length; i++) {
                    element.classList.add(tagClasses[i]);
                }
            }
            // если атрибуты объявлены - добавляем
            if (_isObject(attrs)) {
                for (var key in attrs) {
                    var val = attrs[key];
                    element[key] = val;
                }
            }
            // возвращаем созданый елемент
            if (jQueryElement) {
                return $(element);
            } else {
                return element;
            }
        },

        /* создаем iframe для сео текста */
        _seoBuild = function(wrapper) {
            var seoTimer;
            // создаем iframe, который будет следить за resize'm окна
            var iframe = _crtEl('iframe', false, {id: varSeoIframe, name: varSeoIframe});
            iframe.css({
                'position':'absolute',
                'left':'0',
                'top':'0',
                'width':'100%',
                'height':'100%',
                'z-index':'-1'
            });
            // добавляем его в родитель сео текста
            wrapper.prepend(iframe);
            // "прослушка" ресайза
            seoIframe.onresize = function() {
                clearTimeout(seoTimer);
                seoTimer = setTimeout(function() {
                    wHTML.seoSet();
                }, varSeoDelay);
            };
            // вызываем seoSet()
            wHTML.seoSet();
        };

    /* Публичные методы */

        function Methods(){}

        Methods.prototype = {

            /* установка cео текста на странице */
            seoSet: function() {
                if ($('#'+varSeoTxt).length) {
                    var seoText = $('#'+varSeoTxt);
                    var iframe = seoText.children('#'+varSeoIframe);
                    if (iframe.length) {
                        // если iframe сущствует устанавливаем на место сео текст
                        var seoClone = $('#'+varSeoClone);
                        if (seoClone.length) {
                            // клонеру задаем высоту
                            seoClone.height(seoText.outerHeight(true));
                            // тексту задаем позицию
                            seoText.css({
                                top: seoClone.offset().top
                            });
                        } else {
                            // клонера нету - бьем в колокола !!!
                            console.error('"'+varSeoClone+'" - не найден!');
                        }
                    } else {
                        // если iframe отсутствует, создаем его и устанавливаем на место сео текст
                        _seoBuild(seoText);
                    }
                }
            },

            /* magnificPopup inline */
            mfi: function() {
                $('.mfi').magnificPopup({
                    type: 'inline',
                    closeBtnInside: true,
                    removalDelay: 300,
                    mainClass: 'zoom-in'
                });
            },

            /* magnificPopup ajax */
            mfiAjax: function() {
                $('body').magnificPopup({
                    delegate: '.mfiA',
                    callbacks: {
                        elementParse: function(item) {
                            this.st.ajax.settings = {
                                url: item.el.data('url'),
                                type: 'POST',
                                data: (typeof item.el.data('param') !== 'undefined') ? item.el.data('param') : ''
                            };
                        },
                        ajaxContentAdded: function(el) {
                            wHTML.validation();
                            wHTML.phoneMask();
                            $('.phoneMask').hover(
                                function(){
                                    $(this).parent().find(".placeHold").css("display","none");
                                },
                                function(){
                                    if ($(this).val() != 0 ) {
                                        $(this).parent().find(".placeHold").css("display","none");
                                    } else {
                                        $(this).parent().find(".placeHold").css("display","block");
                                    }
                                });

                        }
                    },
                    closeMarkup: '<button title="%title%" type="button" class="mfp-close"><span class="svgHolder"><svg><use xlink:href="#close"></svg></div></button>',
                    type: 'ajax',
                    removalDelay: 300,
                    mainClass: 'zoom-in'
                });
            },

            /* оборачивание iframe и video для адаптации */
            wTxtIFRAME: function() {
                var list = $('.wTxt').find('iframe').add($('.wTxt').find('video'));
                if (list.length) {
                    // в цикле для каждого
                    for (var i = 0; i < list.length; i++) {
                        var element = list[i];
                        var jqElement = $(element);
                        // если имеет класс ignoreHolder, пропускаем
                        if (jqElement.hasClass('ignoreHolder')) {
                            continue;
                        }
                        if (typeof jqElement.data('wraped') === 'undefined') {
                            // определяем соотношение сторон
                            var ratio = parseFloat((+element.offsetHeight / +element.offsetWidth * 100).toFixed(2));
                            if (isNaN(ratio)) {
                                // страховка 16:9
                                ratio = 56.25;
                            }
                            // назнчаем дату и обрачиваем блоком
                            jqElement.data('wraped', true).wrap('<div class="iframeHolder ratio_' + ratio.toFixed(0) + '" style="padding-top:'+ratio+'%;""></div>');
                        }
                    }
                    // фиксим сео текст
                    this.seoSet();
                }
            },
            phoneMask: function () {
                if ( $('.phoneMask').length ) {
                    $('.phoneMask').inputmask({
                        mask: [{
                            "mask": "\+38\ \(###)\ ###\ ##\ ##"
                        }],
                        definitions: {
                            '#': {
                                validator: "[0-9]",
                                cardinality: 1
                            }
                        },
                        oncomplete: function () {
                            $(this).data('valid', true);
                        },
                        onincomplete: function () {
                            $(this).data('valid', false);
                        }
                    });
                }
            },
            googleMap: function (elem) {
                if ($(elem).length) {
                    var lng = $(elem).data('lng'),
                        lat = $(elem).data('lat'),
                        zoom = $(elem).data('zoom'),
                        infoWindow = $(elem).data("info");
                    zoom = parseInt(zoom) ? parseInt(zoom) : 15;

                    var myLatlng = new google.maps.LatLng(lat, lng);

                    var myOptions = {
                        zoom: zoom,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        scrollwheel: false,
                        disableDoubleClickZoom: true
                    };
                    var map = new google.maps.Map(document.getElementById('map'), myOptions);

                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        icon: "Media/pic/marker.png",
                        title: 'SECURITY GROUP'
                    });

                    var contentString =
                        '<div id="contentInfo">' + infoWindow + '</div>'

                    var infowindow = new google.maps.InfoWindow({
                        content: contentString,
                        maxWidth: 250
                    });

                    google.maps.event.addListener(marker, 'click', function () {
                        infowindow.open(map, marker);
                    });

                    if($("#contentInfo")) {
                        infowindow.open(map, marker);
                    }

                    google.maps.event.addListener(map, 'click', function() {
                        infowindow.close();
                    });

                    google.maps.event.addDomListener(window, "resize", function() {
                        var center = map.getCenter();
                        google.maps.event.trigger(map, "resize");
                        map.setCenter(center);
                    });
                }
            }
        };

    /* Объявление wHTML и базовые свойства */
    var wHTML = $.extend(true, Methods.prototype, {});
    return wHTML;
})(jQuery);
jQuery(document).ready(function($) {
    // поддержка cssanimations
    transitFlag = Modernizr.cssanimations;
    // очитска localStorage
    localStorage.clear();
    // сео текст
    wHTML.seoSet();
    // magnificPopup inline
    wHTML.mfi();
    // magnificPopup ajax
    wHTML.mfiAjax();
    // валидация форм
    wHTML.validation();
    wHTML.googleMap("#map");

    /**
     * Инициализация плагина caroufredSell слайдера на главной
     * @name WDOCS_FROM
     * */
    function sliderItems() {
        if ( $(".wSlider").length ) {

            $('.sliderThumbnail ul li').each(function(i) {
                $(this).addClass('itm' + i);
                $(this).on('click', function() {
                    $('.wSliderItems ul').trigger('slideTo', [i, 0, true]);
                    return false;
                });
            });

            $('.sliderThumbnail ul li.itm0').addClass('selected');

            $('.wSliderItems ul').carouFredSel({
                responsive: true,
                width: "100%",
                height: "variable",
                items: {
                    visible: 1,
                    height: 'variable'
                },
                auto: false,
                scroll: {
                    fx: 'crossfade',
                    items: 1,
                    duration: 800,
                    onBefore: function() {
                        var pos = $(this).triggerHandler('currentPosition');
                        $('.sliderThumbnail ul li').removeClass('selected');
                        $('.sliderThumbnail ul li.itm' + pos).addClass('selected');
                    }
                },
                swipe: { onTouch: true }
            }, {
                transition: transitFlag
            });
        }
    }
    //WDOCS_TO

     sliderItems();

    /**
     * Функция для смены текста в html разметке с data-attr
     * @param {string} x - class or id html markup with data-attr
     * @return {string}
     * */
    function changeTxt(x) {
        var changeTxt;
        changeTxt = x.text();
        x.text(x.attr('data-change')).attr('data-change', changeTxt);
    }
    //WDOCS_TO

    $('.linkFullTxt').click(function(){
        var $cont = $(".innerRight .wTxt");
        if($cont.hasClass("max")) {
            $cont.removeClass("max");
            changeTxt($(this));
        } else {
            $cont.addClass("max");
            changeTxt($(this));
        }
    });

    /**
     * Событие при ктором появляется/скрывается меню
     * @name WDOCS_FROM
     * */
    $('.butMob').on('touchstart mousedown',  function(event) {
        event.preventDefault();
        if ($("body").hasClass('openMenu')) {
            $("body").removeClass('openMenu');
        } else {
            $("body").addClass('openMenu');
        }
    });
    //WDOCS_TO

    /**
     * Событие при ктором скрывается меню
     * @name WDOCS_FROM
     * */
    $('.over').on('click',  function(event) {
        event.preventDefault();
        $("body").removeClass('openMenu');
    });
    //WDOCS_TO

    if ($('.wTxt table').length) {
        $('.wTxt table').wrap('<div style="overflow: auto;"></div>');
    }

    /**
     * Инициализация плагина magnificPopup для открытия группи фотографий в большом размере
     * @name WDOCS_FROM
     * */
    $('.zoom').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        mainClass: 'zoom-in',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            tCounter: '%curr% из %total%'
        }
    });
    //WDOCS_TO

    /**
     * Инициализация плагина caroufredSell карусель для лицензий
     * @name WDOCS_FROM
     * */
    if ($('.wCarouselIn').length) {
        $('.wCarouselIn ul').carouFredSel({
            width: '100%',
            direction: "left",
            auto: false,
            prev: ".prev",
            next: ".next",
            scroll: {
                items: 1,
                easing: "swing",
                duration: 600,
                pauseOnHover: true
            },
            swipe: {onTouch: true}
        }, {
            transition: transitFlag
        });
    }
    //WDOCS_TO

    function scrollToBlock (elem) {
        $('html,body').animate({
            scrollTop: $('.' + $(elem).data('anchor')).offset().top
        }, 1000);
    }

        $(".shem_steps li").click(function() {
            scrollToBlock($(this));
        });

        $(".wrapper_link span").click(function() {
            scrollToBlock($(this));
        });

        $(".link_block").click(function() {
            scrollToBlock($(this));
        });

        $(".sectionOne .col p").click(function() {
            scrollToBlock($(this));
        });

    wHTML.phoneMask();

    $('.phoneMask').hover(
        function(){
            $(this).parent().find(".placeHold").css("display","none");
        },
        function(){
            if ($(this).val() != 0 ) {
                $(this).parent().find(".placeHold").css("display","none");
            } else {
                $(this).parent().find(".placeHold").css("display","block");
            }
        });
    /**
     * Инициализация плагина select 2
     * @name WDOCS_FROM
     * */
    if($('select.customSelect').length) {
        $('select.customSelect').select2({
            minimumResultsForSearch: -1,
            allowClear: true
        }).on("select2-close", function() {
            $(this).valid();
        });
    }
    //WDOCS_TO

    $(window).load(function() {
        // оборачивание iframe и video для адаптации
        wHTML.wTxtIFRAME();
    });
});
//# sourceMappingURL=maps/inits.js.map
