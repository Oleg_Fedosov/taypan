<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(array('name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right')); ?>
    </div>
    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <?php echo \Forms\Builder::bool($obj->status, 'status', 'Показывать на главной странице?'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::textarea(array(
                            'name' => 'FORM[name]',
                            'value' => $obj->name,
                            'rows' => 2,
                            'class' => array('valid'),
                        ), 'Подпись под заголовком'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input(array(
                            'name' => 'FORM[link]',
                            'value' => $obj->link,
                            'class' => array('valid'),
                        ), 'Ссылка'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input(array(
                            'name' => 'FORM[cost]',
                            'value' => $obj->cost,
                            'class' => array('valid'),
                        ), 'Цена'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="widgetContent">
            <div class="form-vertical row-border">
                <div class="form-group">
                    <label class="control-label">Изображение</label>
                    <div class="contentImage">
                        <?php if (is_file( HOST . Core\HTML::media('images/stock/original/'.$obj->image) )): ?>
                            <div class="contentImageView">
                                <a href="<?php echo Core\HTML::media('images/stock/original/'.$obj->image); ?>" class="mfpImage">
                                    <img src="<?php echo Core\HTML::media('images/stock/small/'.$obj->image); ?>" />
                                </a>
                            </div>
                            <div class="contentImageControl">
                                <a class="btn btn-danger" href="/wezom/<?php echo Core\Route::controller(); ?>/delete_image">
                                    <i class="fa-remove"></i>
                                    Удалить изображение
                                </a>
                                <br>
                                <a class="btn btn-warning" href="<?php echo \Core\General::crop('stock', 'small', $obj->image); ?>">
                                    <i class="fa-pencil"></i>
                                    Редактировать
                                </a>
                            </div>
                        <?php else: ?>
                            <input type="file" name="file" />
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php echo \Forms\Form::close(); ?>
