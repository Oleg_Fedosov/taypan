<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(array('name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right')); ?>
    </div>
    <div class="col-md-12">
        <div class="widget">
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <?php echo \Forms\Builder::bool($obj->status); ?>
                    </div>
                    <?php if ($obj->created_at): ?>
                        <div class="form-group">
                            <label class="control-label">Дата</label>
                            <?php echo date( 'd.m.Y H:i:s', $obj->created_at ); ?>
                        </div>
                    <?php endif ?>
                    <div class="form-group">
                        <label class="control-label">Имя</label>
                        <?php echo $obj->name; ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Номер телефона</label>
                        <a href="tel:<?php echo $obj->phone; ?>"><?php echo $obj->phone; ?></a>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Адрес</label>
                        <?php echo $obj->address; ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <?php echo $obj->email; ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Тип бизнеса:</label>
                        <?php echo $obj->type_business; ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Круглосуточная охрана:</label>
                        <?php if ($obj->service1 == 1): ?>
                            <?php echo 'да'; ?>
                        <?php else: ?>
                            <?php echo '---'; ?>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Тревожная кнопка:</label>
                        <?php if ($obj->service2 == 1): ?>
                            <?php echo 'да'; ?>
                        <?php else: ?>
                            <?php echo '---'; ?>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Установка сигнализации:</label>
                        <?php if ($obj->service3 == 1): ?>
                            <?php echo 'да'; ?>
                        <?php else: ?>
                            <?php echo '---'; ?>
                        <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Монтаж видеонаблюдения:</label>
                        <?php if ($obj->service4 == 1): ?>
                            <?php echo 'да'; ?>
                        <?php else: ?>
                            <?php echo '---'; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>