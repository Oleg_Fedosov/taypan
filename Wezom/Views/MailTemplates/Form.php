<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(array('name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right')); ?>
    </div>
    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <label class="control-label">Наименование шаблона</label>
                        <div class="red" style="font-weight: bold;"><?php echo $obj->name; ?></div>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::bool($obj->status); ?>
                    </div>
					<div class="form-group">
                        <?php echo \Forms\Builder::input(array(
                            'name' => 'FORM[subject]',
                            'value' => $obj->subject,
                            'class' => 'valid',
                        ), 'Тема'); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::tiny(array(
                            'name' => 'FORM[text]',
                            'value' => $obj->text,
                        ), 'Шаблон'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="widget">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Переменные
                </div>
            </div>
            <div class="pageInfo alert alert-info">
                <div class="rowSection">
                    <div class="col-md-6"><strong>Доменное имя сайта</strong></div>
                    <div class="col-md-6">{{site}}</div>
                </div>
                <?php if( $obj->id != 15 && $obj->id != 20 && $obj->id != 21 && $obj->id != 26 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>IP</strong></div>
                        <div class="col-md-6">{{ip}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Текущая дата в формате dd.mm.YYYY</strong></div>
                        <div class="col-md-6">{{date}}</div>
                    </div>
                <?php endif; ?>
                <?php if ( $obj->id == 1 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Имя</strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>E-Mail</strong></div>
                        <div class="col-md-6">{{email}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Текст сообщения</strong></div>
                        <div class="col-md-6">{{text}}</div>
                    </div>
                <?php endif ?>
                <?php if ( $obj->id == 27 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Имя</strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Номер телефона</strong></div>
                        <div class="col-md-6">{{phone}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Адрес</strong></div>
                        <div class="col-md-6">{{address}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Номер объекта</strong></div>
                        <div class="col-md-6">{{objectNumber}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Описание заявки</strong></div>
                        <div class="col-md-6">{{text}}</div>
                    </div>
                <?php endif ?>
                <?php if ( $obj->id == 7 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Имя</strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>E-Mail</strong></div>
                        <div class="col-md-6">{{email}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Номер телефона</strong></div>
                        <div class="col-md-6">{{phone}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Адрес</strong></div>
                        <div class="col-md-6">{{address}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Кол-во комнат</strong></div>
                        <div class="col-md-6">{{room}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Кол-во этажей</strong></div>
                        <div class="col-md-6">{{floor}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Кол-во дверей</strong></div>
                        <div class="col-md-6">{{door}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Кол-во окон</strong></div>
                        <div class="col-md-6">{{window}}</div>
                    </div>
                <?php endif ?>
                <?php if ( $obj->id == 8 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Имя</strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>E-Mail</strong></div>
                        <div class="col-md-6">{{email}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Номер телефона</strong></div>
                        <div class="col-md-6">{{phone}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Адрес</strong></div>
                        <div class="col-md-6">{{address}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Кол-во комнат</strong></div>
                        <div class="col-md-6">{{room}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Кол-во этажей</strong></div>
                        <div class="col-md-6">{{floor}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Кол-во окон</strong></div>
                        <div class="col-md-6">{{window}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Наличие балкона</strong></div>
                        <div class="col-md-6">{{balcony}}</div>
                    </div>
                <?php endif ?>
                <?php if ( $obj->id == 9 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Имя</strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>E-Mail</strong></div>
                        <div class="col-md-6">{{email}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Номер телефона</strong></div>
                        <div class="col-md-6">{{phone}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Адрес</strong></div>
                        <div class="col-md-6">{{address}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Тип бизнеса</strong></div>
                        <div class="col-md-6">{{typeBusiness}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Круглосуточная охрана</strong></div>
                        <div class="col-md-6">{{service1}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Тревожная кнопка</strong></div>
                        <div class="col-md-6">{{service2}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Установка сигнализации</strong></div>
                        <div class="col-md-6">{{service3}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Монтаж видеонаблюдения</strong></div>
                        <div class="col-md-6">{{service4}}</div>
                    </div>
                <?php endif ?>
                <?php if ( $obj->id == 2 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Ссылка для отмены рассылки</strong></div>
                        <div class="col-md-6">{{link}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>E-Mail</strong></div>
                        <div class="col-md-6">{{email}}</div>
                    </div>
                <?php endif ?>
                <?php if ( $obj->id == 3 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Имя</strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Номер телефона</strong></div>
                        <div class="col-md-6">{{phone}}</div>
                    </div>
                <?php endif ?>

                <?php if ( $obj->id == 4 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Ссылка для подтверждения</strong></div>
                        <div class="col-md-6">{{link}}</div>
                    </div>
                <?php endif ?>
                <?php if ( $obj->id == 5 OR $obj->id == 6 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Новый пароль для входа</strong></div>
                        <div class="col-md-6">{{password}}</div>
                    </div>
                <?php endif ?>
                <?php if ( $obj->id == 11 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Имя</strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>E-Mail</strong></div>
                        <div class="col-md-6">{{email}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Номер телефона</strong></div>
                        <div class="col-md-6">{{phone}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Адрес</strong></div>
                        <div class="col-md-6">{{address}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Тип бизнеса</strong></div>
                        <div class="col-md-6">{{typeBusiness}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Кол-во человек</strong></div>
                        <div class="col-md-6">{{people}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Срок договора</strong></div>
                        <div class="col-md-6">{{agreement}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Время дежурства</strong></div>
                        <div class="col-md-6">{{onduty}}</div>
                    </div>
                <?php endif ?>
                <?php if ( $obj->id == 20 || $obj->id == 21 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Имя, указанное при заказе</strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Фамилия, указанное при заказе</strong></div>
                        <div class="col-md-6">{{last_name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Отчество, указанное при заказе</strong></div>
                        <div class="col-md-6">{{middle_name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Номер заказа</strong></div>
                        <div class="col-md-6">{{id}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Сумма заказа</strong></div>
                        <div class="col-md-6">{{amount}}</div>
                    </div>
                <?php endif ?>
                <?php if ( $obj->id == 28 ): ?>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Имя</strong></div>
                        <div class="col-md-6">{{name}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>E-Mail</strong></div>
                        <div class="col-md-6">{{email}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Номер телефона</strong></div>
                        <div class="col-md-6">{{phone}}</div>
                    </div>
                    <div class="rowSection">
                        <div class="col-md-6"><strong>Услуга</strong></div>
                        <div class="col-md-6">{{service}}</div>
                    </div>
                <?php endif ?>
            </div>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>