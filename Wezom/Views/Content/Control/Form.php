<?php echo \Forms\Builder::open(); ?>
    <div class="form-actions" style="display: none;">
        <?php echo \Forms\Form::submit(array('name' => 'name', 'value' => 'Отправить', 'class' => 'submit btn btn-primary pull-right')); ?>
    </div>
    <div class="col-md-7">
        <div class="widget box">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Основные данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <label class="control-label">Название</label>
                        <b class="red"><?php echo $obj->name; ?></b>
                    </div>
                    <?php if($obj->alias == 'index' || $obj->alias == 'about'): ?>
                        <div class="form-group">
                            <?php echo \Forms\Builder::input(array(
                                'name' => 'OTHER[caption]',
                                'value' => $other->caption,
                                'class' => array('valid'),
                            ), 'Заголовок'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo \Forms\Builder::textarea(array(
                                'name' => 'OTHER[signature]',
                                'value' => $other->signature,
                                'rows' => 3,
                                'class' => array('valid'),
                            ), 'Подпись под заголовком'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="form-group">
                        <?php echo \Forms\Builder::tiny(array(
                            'name' => 'FORM[text]',
                            'value' => $obj->text,
                        ), 'Контент'); ?>
                    </div>
                    <?php if($obj->alias == 'about'): ?>
                        <div class="form-group">
                            <?php echo \Forms\Builder::tiny(array(
                                'name' => 'OTHER[textf]',
                                'value' => $other->textf,
                                'style' => 'height: 150px;',
                            ), 'Текст 1'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo \Forms\Builder::tiny(array(
                                'name' => 'OTHER[texts]',
                                'value' => $other->texts,
                                'style' => 'height: 150px;',
                            ), 'Текст 2'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo \Forms\Builder::tiny(array(
                                'name' => 'OTHER[textl]',
                                'value' => $other->textl,
                                'style' => 'height: 150px;',
                            ), 'Текст слева'); ?>
                        </div>
                        <div class="form-group">
                            <?php echo \Forms\Builder::tiny(array(
                                'name' => 'OTHER[textr]',
                                'value' => $other->textr,
                                'style' => 'height: 150px;',
                            ), 'Текст справа'); ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="widget">
            <div class="widgetHeader">
                <div class="widgetTitle">
                    <i class="fa-reorder"></i>
                    Мета-данные
                </div>
            </div>
            <div class="widgetContent">
                <div class="form-vertical row-border">
                    <div class="form-group">
                        <?php echo \Forms\Builder::input(array(
                            'name' => 'FORM[h1]',
                            'value' => $obj->h1,
                        ), array(
                            'text' => 'H1',
                            'tooltip' => 'Рекомендуется, чтобы тег h1 содержал ключевую фразу, которая частично или полностью совпадает с title',
                        )); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::input(array(
                            'name' => 'FORM[title]',
                            'value' => $obj->title,
                        ), array(
                            'text' => 'Title',
                            'tooltip' => '<p>Значимая для продвижения часть заголовка должна быть не более 12 слов</p><p>Самые популярные ключевые слова должны идти в самом начале заголовка и уместиться в первых 50 символов, чтобы сохранить привлекательный вид в поисковой выдаче.</p><p>Старайтесь не использовать в заголовке следующие знаки препинания – . ! ? – </p>',
                        )); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::textarea(array(
                            'name' => 'FORM[keywords]',
                            'rows' => 5,
                            'value' => $obj->keywords,
                        ), array(
                            'text' => 'Keywords',
                        )); ?>
                    </div>
                    <div class="form-group">
                        <?php echo \Forms\Builder::textarea(array(
                            'name' => 'FORM[description]',
                            'value' => $obj->description,
                            'rows' => 5,
                        ), array(
                            'text' => 'Description',
                        )); ?>
                    </div>
                </div>
            </div>
            <?php if($obj->alias == 'index' || $obj->alias == 'about'): ?>
                <div class="widgetContent">
                    <div class="form-vertical row-border">
                        <div class="form-group">
                            <label class="control-label">Изображение</label>
                            <div class="contentImage">
                                <?php if (is_file( HOST . Core\HTML::media('images/control/original/'.$obj->image) )): ?>
                                    <div class="contentImageView">
                                        <a href="<?php echo Core\HTML::media('images/control/original/'.$obj->image); ?>" class="mfpImage">
                                            <img src="<?php echo Core\HTML::media('images/control/small/'.$obj->image); ?>" />
                                        </a>
                                    </div>
                                    <div class="contentImageControl">
                                        <a class="btn btn-danger" href="/wezom/<?php echo Core\Route::controller(); ?>/delete_image/<?php echo $obj->id; ?>">
                                            <i class="fa-remove"></i>
                                            Удалить изображение
                                        </a>
                                        <br>
                                        <a class="btn btn-warning" href="<?php echo \Core\General::crop('control', 'small', $obj->image); ?>">
                                            <i class="fa-pencil"></i>
                                            Редактировать
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <input type="file" name="file" />
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif ?>
        </div>
    </div>
<?php echo \Forms\Form::close(); ?>
<?php if($obj->alias == 'about'): ?>
    <?php echo $uploader; ?>
<?php endif ?>
