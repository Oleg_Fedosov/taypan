<?php
    namespace Wezom\Modules\Index\Controllers;

    use Core\QB\DB;
    use Core\View;
    use Modules\News\Models\News;

    class Index extends \Wezom\Modules\Base {

        function indexAction () {
            $this->_seo['h1'] = 'Панель управления';
            $this->_seo['title'] = 'Панель управления';

            $count_contacts = (int) DB::select(array(DB::expr('COUNT(id)'), 'count'))->from('contacts')->where('status', '=', 0)->count_all();
            $count_callbacks = (int) DB::select(array(DB::expr('COUNT(id)'), 'count'))->from('callback')->where('status', '=', 0)->count_all();
            $counts_calltechs = (int) DB::select(array(DB::expr('COUNT(id)'), 'count'))->from('calltech')->where('status', '=', 0)->count_all();
            $news = News::countRows(1);

            $this->_content = View::tpl( array(
                'count_contacts' => $count_contacts,
                'count_callbacks' => $count_callbacks,
                'counts_calltechs' => $counts_calltechs,
                'news' => $news,
            ), 'Index/Main');
        }

    }