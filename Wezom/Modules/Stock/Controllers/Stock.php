<?php
    namespace Wezom\Modules\Stock\Controllers;

    use Core\Route;
    use Core\Widgets;
    use Core\Message;
    use Core\Arr;
    use Core\HTTP;
    use Core\View;

    use Wezom\Modules\Stock\Models\Stock AS Model;

    class Stock extends \Wezom\Modules\Base {

        public $tpl_folder = 'Stock';

        function before() {
            parent::before();
            $this->_seo['h1'] = 'Акции на главной';
            $this->_seo['title'] = 'Акции на главной';
            $this->setBreadcrumbs('Акции на главной', 'wezom/'.Route::controller().'/index');
        }

        function indexAction () {
            $id = 1;
            if ($_POST) {
                $post = $_POST['FORM'];
                $post['status'] = Arr::get( $_POST, 'status', 0 );
                if( Model::valid($post) ) {
                    $res = Model::update($post, $id);
                    if($res !== false) {
                        Model::uploadImage($id);
                        Message::GetMessage(1, 'Вы успешно изменили данные!');
                        HTTP::redirect('wezom/'.Route::controller().'/index');
                    } else {
                        Message::GetMessage(0, 'Не удалось изменить данные!');
                    }
                }
                $result = Arr::to_object($post);
            } else {
                $result = Model::getRow($id);
            }
            $this->_toolbar = Widgets::get('Toolbar/Edit', array('noAdd' => true));
            $this->_content = View::tpl(
                array(
                    'obj' => $result,
                    'tpl_folder' => $this->tpl_folder,
                ), $this->tpl_folder.'/Form');
        }

        function deleteImageAction()
        {
            $id = 1;
            $page = Model::getRow($id);
            if (!$page) {
                Message::GetMessage(0, 'Данные не существуют!');
            }
            Model::deleteImage($page->image, $id);
            Message::GetMessage(1, 'Данные удалены!');
            HTTP::redirect('wezom/' . Route::controller() . '/index');
        }
    }