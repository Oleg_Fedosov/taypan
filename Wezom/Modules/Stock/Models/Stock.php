<?php
    namespace Wezom\Modules\Stock\Models;

    use Core\Arr;
    use Core\Message;

    class Stock extends \Core\Common {

        public static $table = 'stock';
        public static $image = 'stock';
    }