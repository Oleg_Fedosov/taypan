<?php
    namespace Wezom\Modules\Content\Controllers;

    use Core\Route;
    use Core\Widgets;
    use Core\Message;
    use Core\Arr;
    use Core\HTTP;
    use Core\View;

    use Wezom\Modules\Content\Models\Stock AS Model;

    class Stock extends \Wezom\Modules\Base {

        public $tpl_folder = 'Content/Stock';

        function before() {
            parent::before();
            $this->_seo['h1'] = 'Системные страницы';
            $this->_seo['title'] = 'Системные страницы';
            $this->setBreadcrumbs('Системные страницы', 'wezom/'.Route::controller().'/index');
        }

        function indexAction () {
            $result = Model::getRows(NULL, 'name', 'ASC');
            $this->_content = View::tpl(
                array(
                    'result'        => $result,
                    'tpl_folder'    => $this->tpl_folder,
                    'tablename'     => Model::$table,
                ), $this->tpl_folder.'/Index');
        }

        function editAction () {
            if ($_POST) {
                $post = $_POST['FORM'];
                $post['other'] = json_encode($_POST['OTHER']);
                if( Model::valid($post) ) {
                    $res = Model::update($post, Route::param('id'));
                    if($res !== false) {
                        Model::uploadImage(Route::param('id'));
                        Message::GetMessage(1, 'Вы успешно изменили данные!');
                        if(Arr::get($_POST, 'button', 'save') == 'save-close') {
                            HTTP::redirect('wezom/'.Route::controller().'/index');
                        } else {
                            HTTP::redirect('wezom/' . Route::controller() . '/edit/' . Route::param('id'));
                        }
                    } else {
                        Message::GetMessage(0, 'Не удалось изменить данные!');
                    }
                }
                $result = Arr::to_object($post);
                $other = Arr::to_object($_POST['OTHER']);
            } else {
                $result = Model::getRow(Route::param('id'));
                $other = json_decode($result->other);
            }
            $this->_toolbar = Widgets::get('Toolbar/Edit', array('noAdd' => true));
            $this->_seo['h1'] = 'Редактирование';
            $this->_seo['title'] = 'Редактирование';
            $this->setBreadcrumbs('Редактирование', 'wezom/'.Route::controller().'/form');
            $this->_content = View::tpl(
                array(
                    'obj' => $result,
                    'other' => $other,
                    'tpl_folder' => $this->tpl_folder,
                ), $this->tpl_folder.'/Form');
        }

        function deleteImageAction()
        {
            $id = (int)Route::param('id');
            $page = Model::getRow($id);
            if (!$page) {
                Message::GetMessage(0, 'Данные не существуют!');
                HTTP::redirect('wezom/' . Route::controller() . '/index');
            }
            Model::deleteImage($page->image, $id);
            Message::GetMessage(1, 'Данные удалены!');
            HTTP::redirect('wezom/' . Route::controller() . '/edit/' . $id);
        }
    }