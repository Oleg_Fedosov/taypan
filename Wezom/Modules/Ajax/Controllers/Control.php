<?php
namespace Wezom\Modules\Ajax\Controllers;

use Core\Arr;
use Core\HTML;
use Core\View;
use Core\Config;
use Core\Files;
use Wezom\Modules\Content\Models\License as Model;

class Control extends \Wezom\Modules\Ajax
{

    /**
     * Upload photo
     * $this->files['file'] => incoming image
     */
    public function uploadPhotoAction()
    {
        if (empty($this->files['file'])) die('No File!');
        $headers = HTML::emu_getallheaders();
        if (array_key_exists('Upload-Filename', $headers)) {
            $name = $headers['Upload-Filename'];
        } else {
            $name = $this->files['file']['name'];
        }
        $name = explode('.', $name);
        $ext = strtolower(end($name));
        if (!in_array($ext, Config::get('images.types'))) die('Not image!');
        $filename = Files::uploadImage(Model::$image);
        Model::insert(array(
            'image' => $filename
        ));
        $this->success(array(
            'confirm' => true,
        ));
    }


    /**
     * Get album photos list
     */
    public function getUploadedPhotosAction()
    {
        $arr = explode('/', Arr::get($_SERVER, 'HTTP_REFERER'));
        $images = Model::getRows(null, 'sort', 'ASC');
        $show_images = View::tpl(array('images' => $images), 'Content/Control/UploadedImages');
        $this->success(array(
            'images' => $show_images,
            'count' => count($images),
        ));
    }


    /**
     * Delete uploaded photo from album
     * $this->post['id'] => ID from gallery images table in DB
     */
    public function deleteUploadedPhotosAction()
    {
        $id = (int)Arr::get($this->post, 'id');
        if (!$id) die('Error!');
        $image = Model::getRow($id)->image;
        Model::deleteImage($image);
        Model::delete($id);
        $this->success();
    }


    /**
     * Sort photos in current album
     * $this->post['order'] => array with photos IDs in right order
     */
    public function sortPhotosAction()
    {
        $order = Arr::get($this->post, 'order');
        if (!is_array($order)) die('Error!');
        $updated = 0;
        foreach ($order as $key => $value) {
            Model::update(array('sort' => $key + 1), (int) $value);
            $updated++;
        }
        $this->success(array(
            'updated' => $updated,
        ));
    }

}