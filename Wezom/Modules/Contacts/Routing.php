<?php   
    
    return array(
        // Callback
        'wezom/callback/index' => 'contacts/callback/index',
        'wezom/callback/index/page/<page:[0-9]*>' => 'contacts/callback/index',
        'wezom/callback/edit/<id:[0-9]*>' => 'contacts/callback/edit',
        'wezom/callback/delete/<id:[0-9]*>' => 'contacts/callback/delete',
		// Calltech
        'wezom/calltech/index' => 'contacts/calltech/index',
        'wezom/calltech/index/page/<page:[0-9]*>' => 'contacts/calltech/index',
        'wezom/calltech/edit/<id:[0-9]*>' => 'contacts/calltech/edit',
        'wezom/calltech/delete/<id:[0-9]*>' => 'contacts/calltech/delete',
        // Contacts
        'wezom/contacts/index' => 'contacts/contacts/index',
        'wezom/contacts/index/page/<page:[0-9]*>' => 'contacts/contacts/index',
        'wezom/contacts/edit/<id:[0-9]*>' => 'contacts/contacts/edit',
        'wezom/contacts/delete/<id:[0-9]*>' => 'contacts/contacts/delete',
        // Home
        'wezom/home/index' => 'contacts/home/index',
        'wezom/home/index/page/<page:[0-9]*>' => 'contacts/home/index',
        'wezom/home/edit/<id:[0-9]*>' => 'contacts/home/edit',
        'wezom/home/delete/<id:[0-9]*>' => 'contacts/home/delete',
        // Flat
        'wezom/flat/index' => 'contacts/flat/index',
        'wezom/flat/index/page/<page:[0-9]*>' => 'contacts/flat/index',
        'wezom/flat/edit/<id:[0-9]*>' => 'contacts/flat/edit',
        'wezom/flat/delete/<id:[0-9]*>' => 'contacts/flat/delete',
        // Business
        'wezom/business/index' => 'contacts/business/index',
        'wezom/business/index/page/<page:[0-9]*>' => 'contacts/business/index',
        'wezom/business/edit/<id:[0-9]*>' => 'contacts/business/edit',
        'wezom/business/delete/<id:[0-9]*>' => 'contacts/business/delete',
        // Physical
        'wezom/physical/index' => 'contacts/physical/index',
        'wezom/physical/index/page/<page:[0-9]*>' => 'contacts/physical/index',
        'wezom/physical/edit/<id:[0-9]*>' => 'contacts/physical/edit',
        'wezom/physical/delete/<id:[0-9]*>' => 'contacts/physical/delete',
        // Mobile
        'wezom/mobile/index' => 'contacts/mobile/index',
        'wezom/mobile/index/page/<page:[0-9]*>' => 'contacts/mobile/index',
        'wezom/mobile/edit/<id:[0-9]*>' => 'contacts/mobile/edit',
        'wezom/mobile/delete/<id:[0-9]*>' => 'contacts/mobile/delete',
    );