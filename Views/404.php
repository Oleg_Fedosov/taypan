<div class="wSize">
    <div class="wErrorContent">
        <div class="errorTit">404</div>
        <div class="errorContent">
            <h3>Страница не найдена</h3>
            <p>
                <em>К сожалению, страница, которую Вы запросили, не была найдена.</em>
                <br> Вы можете перейти на
                <a href="<?php echo Core\HTML::link(); ?>">главную страницу</a>или ознакомиться с нашими
                <a href="<?php echo Core\HTML::link('service'); ?>">услугами</a>.
            </p>
            <p>В любом случае, у нас всегда есть,
                <a href="<?php echo Core\HTML::link('sitemap'); ?>">что вам предложить</a>.
            </p>
        </div>
    </div>
</div>