<div class="wSize">
    <div class="wrNews">
        <?php foreach ($result as $obj): ?>
            <div class="item">
                <?php if ( is_file( HOST.Core\HTML::media('images/news/small/'. $obj->image)) ): ?>
                    <a href="<?php echo Core\HTML::link('news/' . $obj->alias, true); ?>" class="leftCol">
                        <img src="<?php echo Core\HTML::media('images/news/small/'. $obj->image); ?>" alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>">
                    </a>
                <?php endif ?>
                <div class="rightCol">
                    <?php if ($obj->date): ?>
                        <a href="<?php echo Core\HTML::link('news/' . $obj->alias, true); ?>" class="date">
                            <div class="num"><?php echo date('d', $obj->date); ?></div>
                            <div class="numDtl">
                                <span><?php echo $monthRus[date('n', $obj->date)]; ?></span>
                                <span><?php echo date('Y', $obj->date); ?></span>
                            </div>
                        </a>
                    <?php endif; ?>
                    <a href="<?php echo Core\HTML::link('news/'. $obj->alias, true); ?>" title="<?php echo $obj->name; ?>" class="tit"><?php echo $obj->name; ?></a>
                    <div class="desc"><?php echo Core\Text::limit_words(strip_tags($obj->descript_text), 25); ?></div>
                    <div class="w_tar">
                        <a href="<?php echo Core\HTML::link('news/'. $obj->alias, true); ?>" class="linkReadMore">Подробнее</a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <?php echo $pager; ?>
</div>



