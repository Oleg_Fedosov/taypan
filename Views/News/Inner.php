<div class="wSize">
    <div class="wTxt pageTxt">
        <?php if ($obj->date): ?>
            <div class="date">
                <div class="num"><?php echo date('d', $obj->date); ?></div>
                <div class="numDtl">
                    <span><?php echo $monthRus[date('n', $obj->date)]; ?></span>
                    <span><?php echo date('Y', $obj->date); ?></span>
                </div>
            </div>
        <?php endif; ?>
        <?php if ( is_file(HOST.Core\HTML::media('images/news/big/'. $obj->image)) AND $obj->show_image ): ?>
            <div class="news_img">
                <img src="<?php echo Core\HTML::media('images/news/big/'. $obj->image, true); ?>" alt="<?php echo $obj->name; ?>" title="<?php echo $obj->name; ?>"/>
            </div>
        <?php endif; ?>
        <?php echo $obj->text; ?>
    </div>
    <div class="wlinkBack">
        <a href="<?php echo Core\HTML::link('news', true); ?>">Вернуться к списку новостей</a>
    </div>
</div>


