<div class="wSize">
    <nav class="wSitemap">
        <ul>
            <li>
                <a href="<?php echo Core\HTML::link('/'); ?>">
                    <span>Главная</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Core\HTML::link('contact'); ?>">
                    <span>Контакты</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Core\HTML::link('about'); ?>">
                    <span>О компании</span>
                </a>
            </li>
            <li class="haveSubMenu">
                <a href="<?php echo Core\HTML::link('services'); ?>">
                    <span>Услуги</span>
                </a>
                <?php if (count($services)): ?>
                    <ul>
                        <?php foreach ($services AS $obj): ?>
                            <li>
                                <a href="<?php echo Core\HTML::link('services/' . $obj->alias); ?>">
                                    <span>
                                        <?php echo $obj->name; ?>
                                    </span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </li>
            <li class="haveSubMenu">
                <a href="<?php echo Core\HTML::link('news'); ?>">
                    <span>Новости</span>
                </a>
                <?php if (count($news)): ?>
                    <ul>
                        <?php foreach ($news AS $obj): ?>
                            <li>
                                <a href="<?php echo Core\HTML::link('news/' . $obj->alias); ?>">
                                    <span>
                                        <?php echo $obj->name; ?>
                                    </span>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
            </li>
            <?php foreach ($pages[0] AS $obj): ?>
                <li class="haveSubMenu">
                    <a href="<?php echo Core\HTML::link($obj->alias); ?>">
                        <span><?php echo $obj->name; ?></span>
                    </a>
                    <?php echo Core\View::tpl(array('result' => $pages, 'cur' => $obj->id, 'add' => ''), 'Sitemap/Recursive'); ?>
                </li>
            <?php endforeach; ?>
            <li>
                <a href="<?php echo Core\HTML::link('sitemap'); ?>">
                    <span>Карта сайта</span>
                </a>
            </li>
        </ul>
    </nav>
</div>



