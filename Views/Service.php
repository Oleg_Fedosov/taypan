<!DOCTYPE html>
<html lang="ru-RU" dir="ltr" class="no-js">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Core\Widgets::get('Head', $_seo); ?>
    <?php foreach ( $_seo['scripts']['head'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>
<body class="innerPage">
<?php foreach ( $_seo['scripts']['body'] as $script ): ?>
    <?php echo $script; ?>
<?php endforeach ?>
<div class="wWrapper">
    <?php echo Core\Widgets::get('Header', array('config' => $_config)); ?>
    <div class="wContainer">
        <?php echo $_content; ?>
        <!-- .wConteiner-->
    </div>
</div>
<?php echo Core\Widgets::get('HiddenData'); ?>
<?php echo Core\Widgets::get('Footer', array('counters' => $_seo['scripts']['counter'])); ?>
</body>
</html>