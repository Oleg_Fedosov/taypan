<div class="wPageContatcs">
    <div class="wSize">
        <div class="wrapCol">
            <div class="lCol">
                <div class="ttl">Контактная информация</div>
                <div class="address"><?php echo Core\Config::get('static.office_address'); ?></div>
                <div class="rowCol w_clearfix">
                    <div class="col">
                        <div class="titCol">Основные телефоны</div>
                        <ul>
                            <?php if ($phone1 = trim(Core\Config::get('static.phone1'))): ?>
                                <li>
                                    <a href="tel:<?php echo preg_replace('#\D#', '', $phone1); ?>">
                                        <?php $ph1 = explode(')',  $phone1); ?>
                                        <?php echo '<small>' . $ph1[0] . ')</small>' . $ph1[1]; ?>
                                    </a>
                                </li>
                            <?php endif ?>
                            <?php if ($phone2 = trim(Core\Config::get('static.phone2'))): ?>
                                <li>
                                    <a href="tel:<?php echo preg_replace('#\D#', '', $phone2); ?>">
                                        <?php $ph2 = explode(')',  $phone2); ?>
                                        <?php echo '<small>' . $ph2[0] . ')</small>' . $ph2[1]; ?>
                                    </a>
                                </li>
                            <?php endif ?>
                        </ul>
                    </div>
                    <div class="col">
                        <div class="titCol">Диспетчер</div>
                        <ul>
                            <?php if ($phone3 = trim(Core\Config::get('static.phone3'))): ?>
                                <li>
                                    <a href="tel:<?php echo preg_replace('#\D#', '', $phone3); ?>">
                                        <?php $ph3 = explode(')',  $phone3); ?>
                                        <?php echo '<small>' . $ph3[0] . ')</small>' . $ph3[1]; ?>
                                    </a>
                                </li>
                            <?php endif ?>
                        </ul>
                    </div>
                    <div class="col">
                        <div class="titCol">Отдел договоров</div>
                        <ul>
                            <?php if ($phone4 = trim(Core\Config::get('static.phone4'))): ?>
                                <li>
                                    <a href="tel:<?php echo preg_replace('#\D#', '', $phone4); ?>">
                                        <?php $ph4 = explode(')',  $phone4); ?>
                                        <?php echo '<small>' . $ph4[0] . ')</small>' . $ph4[1]; ?>
                                    </a>
                                </li>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
                <ul class="mail">
                    <li>
                        <a href="mailto:<?php echo Core\Config::get('static.contact_email_1') ?>"><?php echo Core\Config::get('static.contact_email_1') ?></a>
                    </li>
                    <li>
                        <a href="mailto:<?php echo Core\Config::get('static.contact_email_2') ?>"><?php echo Core\Config::get('static.contact_email_2') ?></a>
                    </li>
                </ul>
                <div class="social">
                    <ul>
                        <li>
                            <a href="<?php echo Core\Config::get('socials.vk'); ?>" target="_blank" class="vk">
                                <img src="<?php echo \Core\HTML::media('pic/vk.png'); ?>">
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo Core\Config::get('socials.fb'); ?>" target="_blank" class="fb">
                                <img src="<?php echo \Core\HTML::media('pic/fb.png'); ?>">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="rCol">
                <div class="ttl">Задать вопрос специалисту</div>
                <div class="formContact formStyle">
                    <div data-form="true" data-ajax="contacts" class="wForm wFormDef">
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <input class="wInput" required type="text" data-name="name" name="userName" id="userName" data-rule-word="true" data-rule-minlength="2">
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15">Ваше имя
                                        <tspan>*</tspan>
                                    </text>
                                </svg>
                                <div for="userName" class="inpInfo">Имя *</div>
                            </div>
                        </div>
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <input class="wInput" required type="email" data-name="email" name="userMail" id="userMail" data-rule-email="true">
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15">Контактный email
                                        <tspan>*</tspan>
                                    </text>
                                </svg>
                                <div class="inpInfo">E-mail *</div>
                            </div>
                        </div>
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <textarea class="wTextarea" required type="text" data-name="text" name="userDescription" id="userDescription" data-rule-minlength="10"></textarea>
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15">Ваш вопрос
                                        <tspan>*</tspan>
                                    </text>
                                </svg>
                                <div class="inpInfo">Вопрос *</div>
                            </div>
                        </div>
                        <?php if(array_key_exists('token', $_SESSION)): ?>
                            <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
                        <?php endif; ?>
                        <div class="wFormRow w_last">
                            <button class="wSubmit custBtn">Задать вопрос</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="wMap">
            <div id="map" data-lat="<?php echo Core\Config::get('coordinates.lat'); ?>" data-lng="<?php echo Core\Config::get('coordinates.lng'); ?>"
                 data-zoom="17" data-info="<?php echo Core\Config::get('coordinates.info'); ?>"></div>
        </div>
    </div>
</div>