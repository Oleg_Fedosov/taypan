<section class="headerSection">
    <div class="wSize">
        <?php echo $breadcrumbs; ?>
        <!-- breadcrumbs -->
        <div class="customTitle">
            <?php if (trim($_seo['h1'])): ?>
                <h1><?php echo $_seo['h1']; ?></h1>
            <?php else: ?>
                <h1><?php echo $_seo['name']; ?></h1>
            <?php endif; ?>
        </div>
        <div class="headBlock w_clearfix">
            <div class="lcol">
                <div class="txt"><?php echo $obj->text; ?></div>
            </div>
            <div class="rcol">
                <section>
                    <div class="top_block">Стоимость обслуживания (грн. / час.):</div>
                    <div class="center_block">
                        <div class="left">от</div>
                        <div class="right">
                            <?php echo $obj->cost; ?>
                        </div>
                    </div>
                    <div data-anchor="section_form" class="link_block">Узнать стоимость</div>
                </section>
            </div>
        </div>
        <div class="bottomBlock w_clearfix">
            <div class="col">
                <div class="b_left">
                    <img src="<?php echo Core\HTML::media('pic/incut_icon_6.png', true); ?>">
                </div>
                <div class="b_right">
                    <p>Круглосуточно на дежурстве патрульные машины, которые охватывают все районы города.</p>
                </div>
            </div>
            <div class="col">
                <div class="b_left">
                    <img src="<?php echo Core\HTML::media('pic/respons.png', true); ?>">
                </div>
                <div class="b_right">
                    <p>Мы несём материальную отвественность за ваше имущество!</p>
                </div>
            </div>
            <div class="col">
                <div class="b_left">
                    <img src="<?php echo Core\HTML::media('pic/incut_icon_1.png', true); ?>">
                </div>
                <div class="b_right">
                    <p>Личный состав имеет в своем распоряжении спецсредства для защиты и связи</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section_form">
    <div class="wSize">
        <div class="title_section_form">Быстрый запрос на просчёт стоимости услуг физической охраны</div>
        <div data-form="true" data-ajax="physical" class="wrapper_form wForm wFormDef">
            <div class="w_clearfix">
                <div class="left_col">
                    <div class="name_col">Общая информация</div>
                    <div class="inner_form">
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <input class="wInput" required type="text" data-name="name" name="userName" data-rule-word="true" data-rule-minlength="2">
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15">Ваше имя
                                        <tspan>*</tspan>
                                    </text>
                                </svg>
                                <div for="userName" class="inpInfo">Имя *</div>
                            </div>
                        </div>
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <input class="wInput phoneMask" required type="tel" data-name="phone" name="userPhone" data-rule-phoneua="true">
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15">Контактный телефон
                                        <tspan>*</tspan>
                                    </text>
                                </svg>
                                <div for="userPhone" class="inpInfo">Телефон *</div>
                            </div>
                        </div>
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <input class="wInput" type="email" data-name="email" name="userMail" data-rule-email="true">
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15" fill="#7c7c7c">E-mail</text>
                                </svg>
                                <div class="inpInfo">E-mail</div>
                            </div>
                        </div>
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <input class="wInput" required type="text" data-name="address" name="userAddress">
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15">Улица
                                        <tspan>*</tspan>
                                    </text>
                                </svg>
                                <div for="userAddress" class="inpInfo">Улица *</div>
                            </div>
                        </div>
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <input class="wInput" required type="text" data-name="typeBusiness" name="typeBusiness">
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15">Тип бизнеса
                                        <tspan>*</tspan>
                                    </text>
                                </svg>
                                <div for="typeBusiness" class="inpInfo">Тип бизнеса *</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right_col">
                    <div class="name_col">Дополнительная информация</div>
                    <div class="info_form">Заполнять не обязательно, но если вы заполните специалисты нашей компании смогут сделать более точный расчет стоимости услуг физической охраны.</div>
                    <div class="inner_form">
                        <div class="wFormRow">
                            <label for="people" class="wLabel">Кол-во человек:</label>
                            <div class="wFormInput">
                                <input class="wInput" type="text" data-name="people" name="people" id="people" placeholder="3" data-rule-digits="true" data-msg-digits="Только цифры">
                            </div>
                        </div>
                        <div class="wFormRow">
                            <label for="agreement" class="wLabel">Срок договора:</label>
                            <div class="wFormInput">
                                <input class="wInput" type="text" data-name="agreement" name="agreement" id="agreement" placeholder="1 год">
                            </div>
                        </div>
                        <div class="wFormRow">
                            <label for="onduty" class="wLabel">Время дежурства:</label>
                            <div class="wFormInput">
                                <input class="wInput" type="text" data-name="onduty" name="onduty" id="onduty" placeholder="с 9 до 18">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sign_form">Мы сразу перезвоним и назовем стоимость, затем отправим расчет на почту!</div>
            <?php if(array_key_exists('token', $_SESSION)): ?>
                <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
            <?php endif; ?>
            <div class="wFormRow w_tac">
                <button class="wSubmit custBtn">Отправить запрос</button>
            </div>
        </div>
        <div class="attention"><span> Внимание!</span>  Если вы пользуетесь услугами другой охранной фирмы, у вас есть возможность бесплатно переключиться на пульт нашей компании.</div>
    </div>
</section>

