<div class="pageService">
    <?php $i = 1; foreach ($result as $obj): ?>
        <div class="wRowCol w_clearfix figure<?php echo $i++; ?>">
            <div
                <?php if ( is_file( HOST.Core\HTML::media('images/services/big/'. $obj->image)) ): ?>
                     style="background: url(<?php echo Core\HTML::media('images/services/big/' . $obj->image, true); ?>)"
                <?php endif ?>
                 class="lCol">
            </div>
            <div class="rCol">
                <div class="wTit">
                    <div class="tit">
                        <span title="<?php echo $obj->name; ?>">
                            <?php echo $obj->name; ?>
                        </span>
                    </div>
                </div>
                <div class="txtBlock wTxt">
                    <?php echo $obj->text; ?>
                </div>
                <div class="wLink">
                    <a href="<?php echo Core\HTML::link('services/' . $obj->alias, true); ?>">Узнать подробнее и подключить</a>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>
<div class="wSeoTxt">
    <div class="wSize">
        <div id="cloneSeo"></div>
    </div>
</div>
