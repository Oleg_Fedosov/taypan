<section class="headerSection">
    <div class="wSize">
        <?php echo $breadcrumbs; ?>
        <!-- breadcrumbs -->
        <div class="customTitle">
            <?php if (trim($_seo['h1'])): ?>
                <h1><?php echo $_seo['h1']; ?></h1>
            <?php else: ?>
                <h1><?php echo $_seo['name']; ?></h1>
            <?php endif; ?>
        </div>
        <div class="headBlock w_clearfix">
            <div class="lcol">
                <div class="txt"><?php echo $obj->text; ?></div>
            </div>
            <div class="rcol">
                <section>
                    <div class="top_block">Стоимость обслуживания (грн. / мес.):</div>
                    <div class="center_block">
                        <div class="left">от</div>
                        <div class="right">
                            <?php echo $obj->cost; ?>
                        </div>
                    </div>
                    <div data-anchor="section_form" class="link_block">Узнать стоимость оборудования</div>
                </section>
            </div>
        </div>
        <div class="bottomBlock w_clearfix">
            <div class="col">
                <div class="b_left">
                    <img src="<?php echo Core\HTML::media('pic/fast.png', true); ?>">
                </div>
                <div class="b_right">
                    <p>Быстро</p><span>Среднее время прибытия группы реагирования - 3-5 мин.</span>
                </div>
            </div>
            <div class="col">
                <div class="b_left">
                    <img src="<?php echo Core\HTML::media('pic/respons.png', true); ?>">
                </div>
                <div class="b_right">
                    <p>Ответственно</p><span>Мы несём материальную отвественность за ваше имущество!</span>
                </div>
            </div>
            <div class="col">
                <div class="b_left">
                    <img src="<?php echo Core\HTML::media('pic/qualit.png', true); ?>">
                </div>
                <div class="b_right">
                    <p>Качественно</p>
                    <span>Монтаж оборудования приблизительно 3-4 часа.</span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section_two">
    <div class="wSize">
        <div class="title_section">Как работает пультовая охрана квартиры ?</div>
        <div class="col_section">
            <div class="left_col">
                <ul class="shem_steps">
                    <li data-anchor="step_one">
                        <span>1</span>
                        <p>Датчик на движение и на разбитие стекла</p>
                    </li>
                    <li data-anchor="step_two">
                        <span>2</span>
                        <p>Датчик открытия окон и дверей</p>
                    </li>
                    <li data-anchor="step_three">
                        <span>3</span>
                        <p>Приёмно контрольный прибор GSM</p>
                    </li>
                    <li data-anchor="step_four">
                        <span>4</span>
                        <p>Пульт управления</p>
                    </li>
                </ul>
            </div>
            <div class="right_col">
                <img src="<?php echo Core\HTML::media('pic/shema.png', true); ?>">
                <div class="sign_shem">Стандартная схема расстановки оборудования в доме</div>
            </div>
        </div>
    </div>
</section>
<section class="section_step step_one">
    <div class="wSize">
        <div class="wrapper_colum w_clearfix">
            <div class="left_col">
                <div class="top_block">
                    <div class="l_block">
                        <img src="<?php echo Core\HTML::media('pic/step_one.png', true); ?>">
                    </div>
                    <div class="r_block">Датчики на движение и на разбитие стекла</div>
                </div>
                <div class="bottom_block">
                    <div class="wImg">
                        <img src="<?php echo Core\HTML::media('pic/step_1.jpg', true); ?>">
                    </div>
                </div>
            </div>
            <div class="right_col">
                <ul>
                    <li>Если в квартире сделан ремонт можно установить беспроводные датчики, но они дороже.</li>
                    <li>В целях экономии, можно уменьшить количество датчиков, но мы этого делать крайне не рекомендуем, т.к. может быть большая задержка в получении сигнала тревоги.</li>
                    <li>Вы можете приобрести и установить датчики самостоятельно, но это вряд ли будет стоить вам дешевле, т.к. мы работаем напрямую с заводами-изготовителями и даем лучшие цены!</li>
                    <li>Если в беспроводном датчике садится батарейка, мы увидим сообщения о низком питании датчика и обеспечим своевременную замену.</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="sign_section">Современные датчики движения не чувствительны к домашним животным до 25 кг.</div>
</section>
<section class="section_step step_two">
    <div class="wSize">
        <div class="wrapper_colum w_clearfix">
            <div class="left_col">
                <div class="top_block">
                    <div class="l_block">
                        <img src="<?php echo Core\HTML::media('pic/step_two.png', true); ?>">
                    </div>
                    <div class="r_block">Датчики открытия окон и дверей</div>
                </div>
                <div class="bottom_block">
                    <div class="wImg">
                        <img src="<?php echo Core\HTML::media('pic/step_2.jpg', true); ?>">
                    </div>
                </div>
            </div>
            <div class="right_col">
                <ul>
                    <li>Данные датчики нужны, чтобы максимально сократить время реагирования службы охраны. Если у вас будут стоять только датчики на движение, то у воров будет больше времени, чтобы</li>
                    <li>При наличии датчика открытия окна или двери, сигнал на наш пульт поступает сразу, как только произошел взлом! Таким образом, у нас есть несколько лишних ценных секунд на задержание преступника.</li>
                    <li>Принцип работы очень прост – одна часть датчика ставится на раму окна, вторая на створку. При их размыкании срабатывает сигнализация.</li>
                    <li>Стоят датчики открытия недорого, поэтому мы советуем их поставить на все окна и входные двери.</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="section_step step_three">
    <div class="wSize">
        <div class="wrapper_colum w_clearfix">
            <div class="left_col">
                <div class="top_block">
                    <div class="l_block">
                        <img src="<?php echo Core\HTML::media('pic/step_three.png', true); ?>">
                    </div>
                    <div class="r_block">Приёмно контрольный прибор GSM</div>
                </div>
                <div class="bottom_block">
                    <div class="wImg">
                        <img src="<?php echo Core\HTML::media('pic/step_3.jpg', true); ?>">
                    </div>
                </div>
            </div>
            <div class="right_col">
                <ul>
                    <li>Теперь, для охраны частного дома вам не нужна телефонная линия, за которую нужно дополнительно платить.</li>
                    <li>Отслеживание ведется круглосуточно и ежесекундно!</li>
                    <li>Расходы на мобильную связь входят в абонплату! Ничего доплачивать не нужно!</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="section_step step_four">
    <div class="wSize">
        <div class="wrapper_colum w_clearfix">
            <div class="left_col">
                <div class="top_block">
                    <div class="l_block">
                        <img src="<?php echo Core\HTML::media('pic/step_four.png', true); ?>">
                    </div>
                    <div class="r_block">Пульт управления</div>
                </div>
                <div class="bottom_block">
                    <div class="wImg">
                        <img src="<?php echo Core\HTML::media('pic/step_4.jpg', true); ?>">
                    </div>
                </div>
            </div>
            <div class="right_col">
                <ul>
                    <li>Для того, чтобы поставить дом на охрану, вам нужно закрыть все окна и двери, после чего загорится индикатор «Готов». Далее ввести Ваш код и в течении 30 секунд покинуть помещение.</li>
                    <li>С наружной стороны двери будет установлен индикатор взятия под охрану (красный светодиод). После того, как 30 секунд истекут, он загорится. Это будет свидетельствовать, что дом под охраной!</li>
                    <li>Снятие дома с охраны происходит очень просто. Вы открываете дверь и у вас есть 30 секунд (время можно увеличить) чтобы ввести свой код на пульте. После этого охрана сразу отключается.</li>
                </ul>
            </div>
        </div>
        <div class="wrapper_link"><span data-anchor="section_form">Заказать услугу</span></div>
    </div>
</section>
<section class="section_incut">
    <div class="wSize">
        <div class="customTitle">Что произойдет,<br>если сработает сигнализация?</div>
        <div class="top_block w_clearfix">
            <div class="l_col">
                <div class="icon">
                    <img src="<?php echo Core\HTML::media('pic/clock.png', true); ?>">
                </div>
                <div class="description">
                    <div class="number">3-5<span>минут</span></div>
                    <p>и группа быстрого реагирования возле вашего дома!</p><small>Мы гарантируем самое быстрое прибытие экипажа в городе!</small>
                </div>
            </div>
            <div class="r_col">
                <div class="icon">
                    <img src="<?php echo Core\HTML::media('pic/handcuffs.png', true); ?>">
                </div>
                <div class="description">
                    <p>Злоумышленник будет задержан и доставлен в полицию!</p>
                    <?php if ($obj->cost_if): ?>
                        <small>И все это всего за</small>
                        <div class="number">
                            <?php echo $obj->cost_if; ?>
                            <span>грн./мес.</span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="middle_block">
            <div class="wrapper_colum w_clearfix">
                <div class="col">
                    <div class="b_top">24/7</div>
                    <div class="b_bottom">Круглосуточно на дежурстве патрульные машины, которые охватывают все районы города.</div>
                </div>
                <div class="col">
                    <div class="b_top">
                        <img src="<?php echo Core\HTML::media('pic/incut_icon_1.png', true); ?>">
                    </div>
                    <div class="b_bottom">Личный состав имеет в своем распоряжении спецсредства для защиты и связи</div>
                </div>
                <div class="col">
                    <div class="b_top">
                        <img src="<?php echo Core\HTML::media('pic/incut_icon_2.png', true); ?>">
                    </div>
                    <div class="b_bottom">Один свободный автомобиль всегда находится на подмене, на случай непредвиденной поломки или аварии.</div>
                </div>
                <div class="col">
                    <div class="b_top">
                        <img src="<?php echo Core\HTML::media('pic/incut_icon_3.png', true); ?>">
                    </div>
                    <div class="b_bottom">Наш автопарк состоит из надежных автомобилей, которые регулярно проходят технический осмотр и всегда в идеальном состоянии.</div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section_form">
    <div class="wSize">
        <div class="title_section_form">Быстрый запрос просчета стоимости комплекта оборудования для квартиры</div>
        <div data-form="true" data-ajax="flat" class="wrapper_form wForm wFormDef">
            <div class="w_clearfix">
                <div class="left_col">
                    <div class="name_col">Общая информация</div>
                    <div class="inner_form">
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <input class="wInput" required type="text" data-name="name" name="userName" data-rule-word="true" data-rule-minlength="2">
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15">Ваше имя
                                        <tspan>*</tspan>
                                    </text>
                                </svg>
                                <div for="userName" class="inpInfo">Имя *</div>
                            </div>
                        </div>
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <input class="wInput phoneMask" required type="tel" data-name="phone" name="userPhone" data-rule-phoneua="true">
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15">Контактный телефон
                                        <tspan>*</tspan>
                                    </text>
                                </svg>
                                <div for="userPhone" class="inpInfo">Телефон *</div>
                            </div>
                        </div>
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <input class="wInput" type="email" data-name="email" name="userMail" data-rule-email="true">
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15" fill="#7c7c7c">E-mail</text>
                                </svg>
                                <div class="inpInfo">E-mail</div>
                            </div>
                        </div>
                        <div class="wFormRow">
                            <div class="wFormInput">
                                <input class="wInput" required type="text" data-name="address" name="userAddress">
                                <svg height="30" width="200" class="placeHold">
                                    <text x="0" y="15">Улица
                                        <tspan>*</tspan>
                                    </text>
                                </svg>
                                <div for="userAddress" class="inpInfo">Улица *</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right_col">
                    <div class="name_col">Опишите квартиру</div>
                    <div class="info_form">Заполнять не обязательно, но если вы заполните специалисты нашей компании смогут сделать более точный расчет стоимости оборудования и установки сигнализации.</div>
                    <div class="inner_form">
                        <div class="wFormRow">
                            <label for="room" class="wLabel">Кол-во комнат:</label>
                            <div class="wFormInput">
                                <input class="wInput" type="text" name="room" data-name="room" id="room" placeholder="3" data-rule-digits="true" data-msg-digits="Только цифры">
                            </div>
                        </div>
                        <div class="wFormRow">
                            <label for="floor" class="wLabel">Этаж:</label>
                            <div class="wFormInput">
                                <input class="wInput" type="text" name="floor" data-name="floor" id="floor" placeholder="3" data-rule-digits="true" data-msg-digits="Только цифры">
                            </div>
                        </div>
                        <div class="wFormRow">
                            <label for="window" class="wLabel">Кол-во окон:</label>
                            <div class="wFormInput">
                                <input class="wInput" type="text" name="window" data-name="window" id="window" placeholder="3" data-rule-digits="true" data-msg-digits="Только цифры">
                            </div>
                        </div>
                        <label class="labelCheck">
                            <input data-name="balcony" value="1" type="checkbox">
                            <span>Есть балкон</span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="sign_form">Мы сразу перезвоним и назовем стоимость, затем отправим расчет на почту!</div>
            <?php if(array_key_exists('token', $_SESSION)): ?>
                <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
            <?php endif; ?>
            <div class="wFormRow w_tac">
                <button class="wSubmit custBtn">Отправить запрос</button>
            </div>
        </div>
        <div class="attention"><span> Внимание!</span>  Если вы пользуетесь услугами другой охранной фирмы, у вас есть возможность бесплатно переключиться на пульт нашей компании.</div>
    </div>
</section>
