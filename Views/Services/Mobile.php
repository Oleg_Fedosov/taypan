<div class="customTitle">
    <?php if (trim($_seo['h1'])): ?>
        <h1><?php echo $_seo['h1']; ?></h1>
    <?php else: ?>
        <h1><?php echo $_seo['name']; ?></h1>
    <?php endif; ?>
    <span class="svgHolder">
        <svg>
            <use xlink:href="#reader"/>
        </svg>
    </span>
</div>
<?php echo $breadcrumbs; ?>
<!-- breadcrumbs -->
<section class="sectionOne">
    <div class="wSize">
        <div class="wrapper_colum">
            <div class="col">
                <div class="icon">
                    <img src="<?php echo Core\HTML::media('pic/icon_sg.png', true); ?>">
                </div>
                <p data-anchor="sectionTwo">Моя охрана SG</p>
                <span>(мобильный телохранитель)</span>
            </div>
            <div class="col">
                <div class="icon">
                    <img src="<?php echo Core\HTML::media('pic/icon_grid.png', true); ?>">
                </div>
                <p data-anchor="sectionFive">Phoenix-MK</p>
                <span>(мобильная клавиатура)</span>
            </div>
            <div class="col">
                <div class="icon">
                    <img src="<?php echo Core\HTML::media('pic/icon_grid.png', true); ?>">
                </div>
                <p data-anchor="sectionSix">Phoenix-MK</p>
                <span>(Push-UP сообщения)</span>
            </div>
        </div>
        <div class="select_txt">Приложение «Моя охрана SG» призвано в первую очередь помогать гражданам против которых совершается правонарушение или ставших свидетелями совершающихся противоправных действий</div>
    </div>
</section>
<section class="sectionTwo">
    <div class="wSize">
        <div class="insert_block">
            <div class="customTitle">Моя охрана SG</div>
            <div class="headBlock w_clearfix">
                <div class="lcol">
                    <p>С помощью приложения «МОЯ ОХРАНА » мы сможем обеспечить Вашу безопасность круглосуточно в любой точке города мобильными группами SG и сотрудников МВД.</p>
                    <p>Приложение создано для смартфонов работающих на ОС iOS (версии от 6.0) и Android(версии от 4.0)</p>
                </div>
                <div class="rcol">
                    <section>
                        <div class="top_block">Стоимость обслуживания (грн. / мес.):</div>
                        <div class="center_block">
                            <div class="left">от</div>
                            <div class="right"><?php echo $obj->cost; ?></div>
                        </div>
                        <div data-anchor="section_form" class="link_block">Заказать</div>
                    </section>
                </div>
            </div>
            <div class="bottomBlock w_clearfix">
                <div class="col">
                    <div class="b_left">
                        <img src="<?php echo Core\HTML::media('pic/fast.png', true); ?>">
                    </div>
                    <div class="b_right">
                        <p>Быстро</p><span>Среднее время прибытия группы реагирования - 3-5 мин.</span>
                    </div>
                </div>
                <div class="col">
                    <div class="b_left">
                        <img src="<?php echo Core\HTML::media('pic/gps.png', true); ?>">
                    </div>
                    <div class="b_right">
                        <p>GPS Модуль</p><span>Определение местоположения клиета при помощи GPS</span>
                    </div>
                </div>
                <div class="col">
                    <div class="b_left">
                        <img src="<?php echo Core\HTML::media('pic/touch.png', true); ?>">
                    </div>
                    <div class="b_right">
                        <p>Одно касание</p><span>Одно касание по экрану Вашего смартфона и мы спешим к Вам!</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sectionThree">
    <div class="wSize">
        <div class="titleCustomSmall">Как работает приложение «Моя охрана SG»?</div>
        <div class="row_colum">
            <div class="left_col">
                <img src="<?php echo Core\HTML::media('pic/iphone_1.jpg', true); ?>">
            </div>
            <div class="right_col">
                <ul class="custom_list">
                    <li>В случаи необходимости воспользоватся услугой «Моб.телохронитель» клиент активирует на своем смартфоне приложения «Моя охрана».</li>
                    <li>Программа определяет ваше местоположение по средствам системы глобального позиционирования (GPS), а оператор пульта передает сигнал тревоги ближайшему экипажу.</li>
                </ul>
            </div>
        </div>
        <div class="row_colum">
            <div class="left_col">
                <img src="<?php echo Core\HTML::media('pic/iphone_2.jpg', true); ?>">
            </div>
            <div class="right_col">
                <ul class="custom_list">
                    <li>Сообщение о тревоги поступает на пульт охраны.</li>
                    <li>После чего на место тревоги выезжает группа реагирования, которая отслеживает Ваше местоположение и перемещениями в реальном времени!</li>
                    <li>Запускать приложение очень быстро и удобно, вынеся его на основную панель рабочего стола вашего смартфона.</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section class="sectionTerm">
    <div class="wSize">
        <div class="term">Где бы вы ни находились: дома, в кафе или просто на улице - Вы всегда будете чувствовать себя в безопасности!</div>
    </div>
</section>
<section class="sectionFour">
    <div class="wSize">
        <div class="titleCustomSmall">Как подключиться к услуге ?</div>
        <div class="wrapper_colum w_clearfix">
            <div class="left_col">
                <ul class="custom_list">
                    <li>1. Вы должны скачать и установить бесплатное приложение с App Store или Google Play</li>
                    <li>2. Заключить договор в офисе компании «Тайпан». После этого услуга «мобильный телохранитель» готова к использованию!</li>
                </ul>
                <a href="<?php echo Core\Config::get('static.googleplay'); ?>" class="link_out">
                    <img src="<?php echo Core\HTML::media('pic/google_play.png', true); ?>">
                </a>
                <a href="<?php echo Core\Config::get('static.appstore'); ?>" class="link_out">
                    <img src="<?php echo Core\HTML::media('pic/app_store.png', true); ?>">
                </a>
            </div>
            <div class="right_col">
                <img src="<?php echo Core\HTML::media('pic/group_iphone.png', true); ?>">
            </div>
        </div>
        <div class="sign_section">Выезд «Группы Быстрого Реагирования» осуществляется в пределах зоны обслуживания охранного предприятия «Тайпан» (г. Кривой Рог)</div>
        <div class="wrapper_link"><span data-anchor="section_form">Заказать услугу</span></div>
    </div>
</section>
<section class="section_incut">
    <div class="wSize">
        <div class="customTitle">Что произойдет,<br>если нажать на тревожную кнопку?<span class="svgHolder">
                            <svg>
                                <use xlink:href="#reader"/>
                            </svg></span></div>
        <div class="top_block w_clearfix">
            <div class="l_col">
                <div class="icon">
                    <img src="<?php echo Core\HTML::media('pic/clock.png', true); ?>">
                </div>
                <div class="description">
                    <div class="number">3-5<span>минут</span></div>
                    <p>и группа быстрого реагирования возле вашего дома!</p>
                    <small>Мы гарантируем самое быстрое прибытие экипажа в городе!</small>
                </div>
            </div>
            <div class="r_col">
                <div class="icon">
                    <img src="<?php echo Core\HTML::media('pic/handcuffs.png', true); ?>">
                </div>
                <div class="description">
                    <p>Злоумышленник будет задержан и доставлен в полицию!</p>
                    <?php if ($obj->cost_if): ?>
                        <small>И все это всего за</small>
                        <div class="number">
                            <?php echo $obj->cost_if; ?>
                            <span>грн./мес.</span>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="middle_block">
            <div class="wrapper_colum w_clearfix">
                <div class="col">
                    <div class="b_top">24/7</div>
                    <div class="b_bottom">Круглосуточно на дежурстве патрульные машины, которые охватывают все районы города.</div>
                </div>
                <div class="col">
                    <div class="b_top">
                        <img src="<?php echo Core\HTML::media('pic/incut_icon_1.png', true); ?>">
                    </div>
                    <div class="b_bottom">Личный состав имеет в своем распоряжении спецсредства для защиты и связи</div>
                </div>
                <div class="col">
                    <div class="b_top">
                        <img src="<?php echo Core\HTML::media('pic/incut_icon_2.png', true); ?>">
                    </div>
                    <div class="b_bottom">Один свободный автомобиль всегда находится на подмене, на случай непредвиденной поломки или аварии.</div>
                </div>
                <div class="col">
                    <div class="b_top">
                        <img src="<?php echo Core\HTML::media('pic/incut_icon_3.png', true); ?>">
                    </div>
                    <div class="b_bottom">Наш автопарк состоит из надежных автомобилей, которые регулярно проходят технический осмотр и всегда в идеальном состоянии.</div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section_form section_form_mobail">
    <div class="wSize">
        <div class="title_section_form">Быстрый запрос на подключение услуги</div>
        <div data-form="true" data-ajax="mobile" class="wrapper_form wForm wFormDef">
            <div class="inner_form">
                <div class="wFormRow">
                    <div class="wFormInput">
                        <input class="wInput" required type="text" data-name="name" name="userName" id="userName" data-rule-word="true" data-rule-minlength="2">
                        <svg height="30" width="200" class="placeHold">
                            <text x="0" y="15">Ваше имя
                                <tspan>*</tspan>
                            </text>
                        </svg>
                        <div for="userName" class="inpInfo">Имя *</div>
                    </div>
                </div>
                <div class="wFormRow">
                    <div class="wFormInput">
                        <input class="wInput phoneMask" required type="tel" data-name="phone" name="userPhone" data-rule-phoneua="true">
                        <svg height="30" width="200" class="placeHold">
                            <text x="0" y="15">Контактный телефон
                                <tspan>*</tspan>
                            </text>
                        </svg>
                        <div for="userPhone" class="inpInfo">Телефон *</div>
                    </div>
                </div>
                <div class="wFormRow">
                    <div class="wFormInput">
                        <input class="wInput" type="email" data-name="email" name="userMail" id="userMail" data-rule-email="true">
                        <svg height="30" width="200" class="placeHold">
                            <text x="0" y="15" fill="#7c7c7c">E-mail</text>
                        </svg>
                        <div class="inpInfo">E-mail</div>
                    </div>
                </div>
                <div class="wFormRow">
                    <div class="wFormInput">
                        <select required data-msg-required="Это поле необходимо заполнить!" data-name="service" name="serviceUser" id="serviceUser" class="customSelect">
                            <option value="">Выберите услугу</option>
                            <option value="1">Моя охрана SG(мобильный телохранитель)</option>
                            <option value="2">Phoenix-MK (Мобильная клавиатура)</option>
                            <option value="3">Phoenix-MK (Push-UP сообщения)</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="sign_form">Наши менеджеры вам сразу перезвонят и назначат встречу, предварительно подготовив документы на Ваше имя!</div>
            <?php if(array_key_exists('token', $_SESSION)): ?>
                <input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
            <?php endif; ?>
            <div class="wFormRow w_tac">
                <button class="wSubmit custBtn">Отправить запрос</button>
            </div>
        </div>
    </div>
</section>
<section class="sectionFive">
    <div class="wSize">
        <div class="insert_block">
            <div class="headTit">Приложение «Phoenix-MK (Мобильная клавиатура)»</div>
            <div class="headBlock w_clearfix">
                <div class="lcol">
                    <p>Приложение «Phoenix-MK» (Мобильная клавиатура) предоставляет дополнительные возможности по управлению удаленным объектом охраны.</p>
                    <p>Оно не заменяет штатные устройства постановки снятия с охраны – считыватели ключей, RFID-меток и клавиатуры.</p>
                </div>
                <div class="rcol">
                    <section>
                        <div class="top_block">Стоимость обслуживания (грн. / мес.):</div>
                        <div class="center_block">
                            <div class="left">от</div>
                            <div class="right"><?php echo $obj->cost1; ?></div>
                        </div>
                        <div data-anchor="section_form" class="link_block">Заказать</div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="sectionSix">
    <div class="wSize">
        <div class="insert_block">
            <div class="headTit">Приложение «Phoenix-MK (Push-UP сообщения)»</div>
            <div class="headBlock w_clearfix">
                <div class="lcol">
                    <p>Для функционирования в мобильном терминале Пользователя должны быть активны GPS (наилучшее качество) и услуга мобильного Internet. Желательно иметь активный статус WiFi и А-GPS.</p>
                    <p>Данные опции могут быть с целью экономии ресурса аккумулятора мобильного устройства включаемы/выключаемы по необходимости, но следует помнить, что наличие их активного статуса влияет на точность позиционирования и как следствие – время оказание помощи.</p>
                </div>
                <div class="rcol">
                    <section>
                        <div class="top_block">Стоимость обслуживания (грн. / мес.):</div>
                        <div class="center_block">
                            <div class="left">от</div>
                            <div class="right"><?php echo $obj->cost2; ?></div>
                        </div>
                        <div data-anchor="section_form" class="link_block">Заказать</div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>
