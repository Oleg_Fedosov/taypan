<!DOCTYPE html>
<html lang="ru-RU" dir="ltr" class="no-js">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Core\Widgets::get('Head', $_seo); ?>
    <?php foreach ( $_seo['scripts']['head'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>
<body class="indexPage">
    <div class="wWrapper">
        <?php echo Core\Widgets::get('Header'); ?>
        <div class="wContainer">
            <?php echo Core\Widgets::get('Index_Slider'); ?>
            <?php echo Core\Widgets::get('Index_Choose'); ?>
            <?php echo Core\Widgets::get('Index_News'); ?>
            <?php echo Core\Widgets::get('Index_Stock'); ?>
            <?php echo Core\Widgets::get('Index_About', ['content' => $_content, 'other' => $other, 'image' => $image]); ?>
            <?php echo Core\Widgets::get('Index_Contact'); ?>
            <!-- .wConteiner-->
        </div>
    </div>
    <?php echo Core\Widgets::get('HiddenData'); ?>
    <?php echo Core\Widgets::get('Footer', array('counters' => $_seo['scripts']['counter'])); ?>
</body>
</html>