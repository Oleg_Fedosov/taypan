<div class="wrIndexNews">
    <div class="wSize">
        <div class="customTitle">Новости компании<span class="svgHolder">
								<svg>
									<use xlink:href="#reader"/>
								</svg></span></div>
        <div class="wItemNews">
            <?php foreach ($result as $obj): ?>
                <div class="item">
                    <div class="top">
                        <div class="date">
                            <span><?php echo date('d', $obj->date); ?></span>
                            <p><?php echo $monthRus[date('n', $obj->date)]; ?></p>
                        </div>
                        <div class="link">
                            <a href="<?php echo Core\HTML::link('news/'. $obj->alias, true); ?>"><?php echo $obj->name; ?></a>
                        </div>
                    </div>
                    <div class="bottom"><?php echo $obj->descript_text; ?></div>
                </div>
            <?php endforeach; ?>
        </div>
        <div class="wLinkArchive">
            <a href="<?php echo Core\HTML::link('news', true); ?>" class="linkArchiv">Архив новостей</a>
        </div>
    </div>
</div>
