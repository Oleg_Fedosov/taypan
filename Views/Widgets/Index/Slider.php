<div class="wSlider">
    <div class="wSliderItems">
        <div class="wSliderIn">
            <ul>
                <?php foreach ($result as $obj): ?>
                    <?php if (is_file(HOST.Core\HTML::media('images/slider/big/'.$obj->image)) && !empty($obj->name)): ?>
                        <li style="background: url(<?php echo Core\HTML::media('images/slider/big/'. $obj->image); ?>)">
                            <div class="wSize">
                                <div class="inner">
                                    <div class="tit big"><?php echo $obj->name; ?></div>
                                    <div class="tit small"><?php echo $obj->subtitle; ?></div>
                                    <div class="wrLinkMore"><a href="<?php echo Core\HTML::link($obj->url); ?>">Подробнее</a></div>
                                </div>
                            </div>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <div class="sliderThumbnail">
        <ul>
            <?php foreach ($result as $obj): ?>
                <?php if (is_file(HOST.Core\HTML::media('images/slider/big/'.$obj->image)) && !empty($obj->name)): ?>
                    <li>
                        <div class="icon">
                            <div class="<?php echo $obj->class; ?>"></div>
                        </div>
                        <div class="name"><?php echo nl2br($obj->name); ?></div>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>