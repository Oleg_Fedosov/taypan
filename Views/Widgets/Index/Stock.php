<div
    <?php if (is_file(HOST.Core\HTML::media('images/stock/big/'. $obj->image))): ?>
        style="background: url(<?php echo Core\HTML::media('images/stock/big/'. $obj->image, true); ?>"
    <?php endif; ?>
        class="wStockIndex">
    <div class="wSize">
        <div class="customTitle"></div>
        <div class="wrCol w_clearfix">
            <div class="lCol">
                <?php $name = explode("\n", $obj->name); ?>
                <p><?php echo $name[0]; ?></p>
                <p><?php echo $name[1]; ?></p>
            </div>
            <div class="rCol">
                <div class="price"><?php echo $obj->cost; ?></div>
                <div class="priceMark">
                    <span>грн.</span><span>мес.</span>
                </div>
            </div>
        </div>
        <div class="wrLinkMore">
            <a href="<?php echo Core\HTML::link($obj->link, true); ?>">Подключить</a>
        </div>
    </div>
</div>