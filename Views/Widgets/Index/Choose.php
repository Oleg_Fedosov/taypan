<div class="chooseIndex">
    <div class="wSize">
        <div class="customTitle">Почему нас выбирают?<span class="svgHolder">
								<svg>
									<use xlink:href="#reader"/>
								</svg></span></div>
        <div class="wrItemChoose">
            <div class="item">
                <img src="<?php echo Core\HTML::media('pic/w_choose_1.png'); ?>">
                <div class="name">Оперативность</div>
                <div class="desc">Способность быстро и правильно осуществлять практические задачи. Скорость – визитная карточка охранного агентства  «Тайпан».</div>
            </div>
            <div class="item">
                <img src="<?php echo Core\HTML::media('pic/w_choose_2.png'); ?>">
                <div class="name">Качество</div>
                <div class="desc">Мы гарантируем клиенту качественное оборудование, и оно будет таковым. И мы несем ответственность за все неисправности оборудования.</div>
            </div>
            <div class="item">
                <img src="<?php echo Core\HTML::media('pic/w_choose_3.png'); ?>">
                <div class="name">Многолетний опыт</div>
                <div class="desc">Наша компания существует с 2002 года. Нам доверили свою безопасность более 1 500 клиентов. За это время было пресечено более 10 000 попыток правонарушений.</div>
            </div>
        </div>
    </div>
</div>