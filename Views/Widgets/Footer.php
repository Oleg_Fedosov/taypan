<footer class="wFooter">
    <div class="wSize">
        <div class="wFooterIn w_clearfix">
            <div class="col_1">
                <div class="copy"><?php echo Core\Config::get('static.copyright') ?></div>
            </div>
            <div class="col_2">
                <ul class="footerMenu">
                    <?php foreach ($contentMenu as $key => $obj): ?>
                        <li>
                            <a href="<?php echo Core\HTML::link($obj->url, true); ?>"><?php echo $obj->name; ?></a>
                        </li>
                    <?php endforeach ?>
                    <li>
                        <a href="<?php echo Core\HTML::link('sitemap', true); ?>">Карта сайта</a>
                    </li>
                </ul>
            </div>
            <div class="col_3">
                <div class="develop">
                    <span class="svgHolder">
                        <svg>
                            <use xlink:href="#icon_wezom"/>
                        </svg>
                    </span>
                    <span class="nameDevelop">Разработка сайта<a href="http://wezom.com.ua/" target="_blank" class="link">студия wezom</a></span>
                </div>
            </div>
            <?php if (isset($counters)): ?>
                <?php foreach ($counters as $counter): ?>
                    <?php echo $counter; ?>
                <?php endforeach ?>
            <?php endif ?>
        </div>
    </div>
</footer>
<!-- .wFooter-->
<div class="wrapMobBlock">
    <div class="mobBlock">
        <div class="nameSite">
            <div class="mainTxt"><?php echo Core\Config::get('basic.name_site') ?></div>
            <div class="subTxt"><?php echo Core\Config::get('basic.subtitle_name_site') ?></div>
        </div>
        <ul class="mMenu">
            <?php foreach ($contentMenu as $key => $obj): ?>
                <li>
                    <a href="<?php echo Core\HTML::link($obj->url, true); ?>"><?php echo $obj->name; ?></a>
                </li>
            <?php endforeach ?>
            <li>
                <a href="<?php echo Core\HTML::link('sitemap', true); ?>">Карта сайта</a>
            </li>
        </ul>
        <div class="mobPhone">
            <ul>
                <?php if ($phone1 = trim(Core\Config::get('static.phone1'))): ?>
                    <li>
                        <a href="tel:<?php echo preg_replace('#\D#', '', $phone1); ?>">
                            <?php $ph1 = explode(')',  $phone1); ?>
                            <?php echo '<small>' . $ph1[0] . ')</small>' . $ph1[1]; ?>
                        </a>
                    </li>
                <?php endif ?>
                <?php if ($phone2 = trim(Core\Config::get('static.phone2'))): ?>
                    <li>
                        <a href="tel:<?php echo preg_replace('#\D#', '', $phone2); ?>">
                            <?php $ph2 = explode(')',  $phone2); ?>
                            <?php echo '<small>' . $ph2[0] . ')</small>' . $ph2[1]; ?>
                        </a>
                    </li>
                <?php endif ?>
            </ul>
        </div>
        <div class="mobSocial">
            <ul>
                <li>
                    <a href="<?php echo Core\Config::get( 'socials.vk'); ?>" target="_blank" class="vk">
                        <img src="<?php echo \Core\HTML::media('pic/vk.png'); ?>">
                    </a>
                </li>
                <li>
                    <a href="<?php echo Core\Config::get( 'socials.fb'); ?>" target="_blank" class="fb">
                        <img src="<?php echo \Core\HTML::media('pic/fb.png'); ?>">
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="over"></div>
<!-- WezomDefs -->
<!-- script disabled message -->