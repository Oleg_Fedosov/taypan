<header class="mobailHeader">
    <div class="butMob">
        <div class="butLines"></div>
    </div>
    <div data-url="<?php echo Core\HTML::link('ajax/popup') ?>" data-param="{&quot;template&quot;:&quot;payment&quot;}" class="mfiA butLeft">
        <div class="svgHolder">
            <svg>
                <use xlink:href="#payment"/>
            </svg>
        </div>
    </div>
    <div class="mobLogo">
        <a href="/">
            <img src="<?php echo Core\HTML::media('pic/site_logo.png'); ?>">
        </a>            
    </div>
    <div data-url="<?php echo Core\HTML::link('ajax/popup') ?>" data-param="{&quot;template&quot;:&quot;callback&quot;}" class="mfiA butRight">
        <div class="svgHolder">
            <svg>
                <use xlink:href="#phone"/>
            </svg>
        </div>
    </div>
    <div data-url="<?php echo Core\HTML::link('ajax/popup') ?>" data-param="{&quot;template&quot;:&quot;callTechnical&quot;}" class="mfiA butRight">
        <div class="svgHolder">
            <svg>
                <use xlink:href="#support"/>
            </svg>
        </div>
    </div>
</header>
<header class="wHeader">
    <div class="wHeaderTop">
        <div class="wSize">
            <?php if (Core\Route::controller() == 'index') : ?>
                <div class="siteLogo">
					<img src="<?php echo Core\HTML::media('pic/site_logo.png'); ?>" alt="">
				</div>
            <?php else: ?>
                <a class="siteLogo" href="<?php echo Core\HTML::link('', true); ?>">
                    <img src="<?php echo Core\HTML::media('pic/site_logo.png'); ?>">
                </a>
            <?php endif; ?>
            <div class="nameSite">
                <div class="mainTxt"><?php echo Core\Config::get('basic.name_site') ?></div>
                <div class="subTxt"><?php echo Core\Config::get('basic.subtitle_name_site') ?></div>
            </div>
            <div class="headerPhone">
                <ul>
                    <?php if ($phone1 = trim(Core\Config::get('static.phone1'))): ?>
                        <li>
                            <a href="tel:<?php echo preg_replace('#\D#', '', $phone1); ?>">
                                <?php $ph1 = explode(')',  $phone1); ?>
                                <?php echo '<small>' . $ph1[0] . ')</small>' . $ph1[1]; ?>
                            </a>
                        </li>
                    <?php endif ?>
                    <?php if ($phone2 = trim(Core\Config::get('static.phone2'))): ?>
                        <li>
                            <a href="tel:<?php echo preg_replace('#\D#', '', $phone2); ?>">
                                <?php $ph2 = explode(')',  $phone2); ?>
                                <?php echo '<small>' . $ph2[0] . ')</small>' . $ph2[1]; ?>
                            </a>
                        </li>
                    <?php endif ?>
                </ul>
            </div>
            <div data-url="<?php echo Core\HTML::link('ajax/popup') ?>" data-param="{&quot;template&quot;:&quot;callback&quot;}"
                 class="mfiA callLink">Обратный звонок</div>
            <div class="socialLink">
                <ul>
                    <li>
                        <a href="<?php echo Core\Config::get('socials.vk'); ?>" target="_blank" class="vk">
                            <img src="<?php echo \Core\HTML::media('pic/vk.png'); ?>">
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo Core\Config::get('socials.fb'); ?>" target="_blank" class="fb">
                        <img src="<?php echo \Core\HTML::media('pic/fb.png'); ?>">
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="wHeaderBottom">
        <div class="wSize">
            <nav class="wLeftCol">
                <ul>
                    <?php foreach ($contentMenu as $key => $obj): ?>
                        <li>
                        <?php if (($obj->url != '/' && strpos($_SERVER['REQUEST_URI'], $obj->url) !== false)
                            || ($obj->url == '/' && Core\Route::controller() == 'index')): ?>
                            <a class="active" href="<?php echo Core\HTML::link($obj->url, true); ?>">
                        <?php else: ?>
                            <a href="<?php echo Core\HTML::link($obj->url, true); ?>">
                        <?php endif; ?>
                                <?php echo $obj->name; ?>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </nav>
            <div class="wRightCol">
                <div data-url="<?php echo Core\HTML::link('ajax/popup') ?>" data-param="{&quot;template&quot;:&quot;callTechnical&quot;}"
                     class="mfiA butTechnical"><span>Вызов инженера</span></div>
                <div data-url="<?php echo Core\HTML::link('ajax/popup') ?>" data-param="{&quot;template&quot;:&quot;payment&quot;}"
                     class="mfiA butPayment"><span>Оплатить услуги охраны</span></div>
            </div>
        </div>
    </div>
</header>
<!-- .wHeader-->
