<div class="wCarousel">
    <div class="wSize">
        <div class="titCarousel">Наши лицензии</div>
        <div class="wCarouselIn">
            <ul>
                <?php foreach ($result as $obj): ?>
                    <?php if (is_file(HOST.Core\HTML::media('images/license/medium/' . $obj->image))): ?>
                        <li>
                            <a href="<?php echo Core\HTML::media('images/license/big/' . $obj->image); ?>" class="zoom">
                                <img src="<?php echo Core\HTML::media('images/license/medium/' . $obj->image); ?>">
                            </a>
                        </li>
                    <?php endif; ?>
                <?php endforeach; ?>
            </ul>
        </div><a href="#" class="prev">
            <div class="svgHolder">
                <svg>
                    <use xlink:href="#prev"/>
                </svg>
            </div></a><a href="#" class="next">
            <div class="svgHolder">
                <svg>
                    <use xlink:href="#next"/>
                </svg>
            </div></a>
    </div>
</div>