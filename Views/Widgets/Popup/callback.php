<div class="mfiModal zoomAnim formStyle">
	<div class="titleForm">Обратный звонок</div>
	<div data-form="true" data-ajax="callback" class="wForm wFormDef">
		<div class="wrCol w_clearfix">
			<div class="lCol">
				<div class="wFormRow">
					<div class="wFormInput">
						<input class="wInput" required="required" type="text" name="userName" id="userName" data-name="name" data-rule-word="true" data-rule-minlength='2'/>
						<svg height="30" width="200" class="placeHold">
							<text x="0" y="15">Ваше имя
								<tspan>*</tspan>
							</text>
						</svg>
						<div for="userName" class="inpInfo">Имя *</div>
					</div>
				</div>
			</div>
			<div class="rCol">
				<div class="wFormRow">
					<div class="wFormInput">
						<input class="wInput phoneMask" required="required" type="tel" data-name="phone" name="userPhone" id="userPhone" data-rule-phoneUA="true"/>
						<svg height="30" width="200" class="placeHold">
							<text x="0" y="15">Контактный телефон
								<tspan>*</tspan>
							</text>
						</svg>
						<div for="userPhone" class="inpInfo">Телефон *</div>
					</div>
				</div>
			</div>
		</div>
		<div class="wFormRow w_tac">
			<div class="infoRequired">* - обязательные для заполнения поля</div>
		</div>
		<?php if(array_key_exists('token', $_SESSION)): ?>
			<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
		<?php endif; ?>
		<div class="wFormRow w_last w_tac">
			<button class="wSubmit custBtn">Заказать звонок</button>
		</div>
	</div>
</div>