
<div class="mfiModal zoomAnim formPayment">
	<div class="titleForm">Для оплаты за услуги выберите получателя</div>
	<div data-form="true" data-ajax="payment" class="wForm wFormDef">
		<label class="labelCheck">
			<input name="tst" data-name="payment" value="t" type="radio" required="required" data-msg-required="Выбирете один из пунктов"/><span>Охранное агенство "Тайпан"</span>
		</label>
		<label class="labelCheck">
			<input name="tst" data-name="payment" value="c" type="radio" required="required"/><span>Агенство охраны "Цербер"</span>
		</label>
		<?php if(array_key_exists('token', $_SESSION)): ?>
			<input type="hidden" data-name="token" value="<?php echo $_SESSION['token']; ?>" />
		<?php endif; ?>
		<div class="wFormRow w_last w_tac">
			<button class="wSubmit custBtn">Оплатить</button>
		</div>
	</div>
</div>