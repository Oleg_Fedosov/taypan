<div class="wPageAbout">
    <div class="wRowCol w_clearfix">
        <div
            <?php if (is_file(HOST.Core\HTML::media('images/control/big/'.$image))): ?>
                style="background: url(<?php echo Core\HTML::media('images/control/big/'. $image, true); ?>)" class="lCol"></div>
        <?php endif; ?>
        <div class="rCol">
            <div class="innerRight">
                <div class="customTitle"><?php echo $other['caption']; ?></div>
                <div class="selectTxt"><?php echo $other['signature']; ?></div>
                <div class="wTxt">
                    <?php echo $other['textf']; ?>
                </div>
            </div>
        </div>
    </div>
    <?php echo Core\Widgets::get('About_Slider'); ?>
    <div class="wSize">
        <div class="wTxt">
            <?php echo $other['texts']; ?>
            <div class="wrTxtRow">
                <div class="col">
                    <div class="l-col">
                        <img src="<?php echo Core\HTML::media('pic/about_icon.png', true); ?>">
                    </div>
                    <div class="r-col">
                        <?php echo $other['textl']; ?>
                    </div>
                </div>
                <div class="col">
                    <div class="l-col">
                        <img src="<?php echo Core\HTML::media('pic/about_icon_2.png', true); ?>">
                    </div>
                    <div class="r-col">
                        <?php echo $other['textr']; ?>
                    </div>
                </div>
            </div>
            <?php echo $text; ?>
        </div>
    </div>
</div>