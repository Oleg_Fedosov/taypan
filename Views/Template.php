<!DOCTYPE html>
<html lang="ru-RU" dir="ltr" class="no-js">
<!-- (c) студия Wezom | www.wezom.com.ua -->
<head>
    <?php echo Core\Widgets::get('Head', $_seo); ?>
    <?php foreach ( $_seo['scripts']['head'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php echo $GLOBAL_MESSAGE; ?>
</head>
<body class="innerPage">
    <?php foreach ( $_seo['scripts']['body'] as $script ): ?>
        <?php echo $script; ?>
    <?php endforeach ?>
    <?php  if (Core\Route::controller() == 'services' && Core\Route::action() == 'index'): ?>
        <div id="seoTxt" class="seoTxt">
            <div class="wSize">
                <div class="wTxt">
                    <?php echo $current->text; ?>
                </div>
            </div>
        </div>
    <?php  endif ?>
    <div class="wWrapper">
        <?php echo Core\Widgets::get('Header', array('config' => $_config)); ?>
        <div class="wContainer">
            <div class="customTitle">
                <?php if (trim($_seo['h1'])): ?>
                    <h1><?php echo $_seo['h1'] ?></h1>
                <?php else: ?>
                    <h1><?php echo $_seo['name'] ?></h1>
                <?php endif; ?>
                <?php if (!Core\Config::get('error')): ?>
                    <span class="svgHolder">
						<svg>
							<use xlink:href="#reader"/>
						</svg>
                    </span>
                <?php endif ?>
            </div>
            <?php echo $_breadcrumbs; ?>
            <?php echo $_content; ?>
            <!-- .wConteiner-->
        </div>
    </div>
    <?php echo Core\Widgets::get('HiddenData'); ?>
    <?php echo Core\Widgets::get('Footer', array('counters' => $_seo['scripts']['counter'])); ?>

</body>
</html>